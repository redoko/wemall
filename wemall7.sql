-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 2017-05-27 10:28:39
-- 服务器版本： 5.5.42
-- PHP Version: 5.5.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wemall7`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `group_id` int(11) NOT NULL DEFAULT '1' COMMENT '用户组id',
  `username` char(16) DEFAULT NULL COMMENT '用户名',
  `password` char(32) DEFAULT NULL COMMENT '密码',
  `email` char(32) DEFAULT NULL COMMENT '用户邮箱',
  `mobile` char(15) DEFAULT NULL COMMENT '用户手机',
  `reg_ip` varchar(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_time` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` text COMMENT '最后登录IP',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态',
  `remark` text COMMENT '备注',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `group_id`, `username`, `password`, `email`, `mobile`, `reg_ip`, `last_login_time`, `last_login_ip`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '296720094@qq.com', '18053449656', '0', '2017-05-27 01:45:05', '0.0.0.0', 1, '', '0000-00-00 00:00:00', '2017-05-27 01:45:05'),
(2, 1, '1', 'c4ca4238a0b923820dcc509a6f75849b', '1604583867@qq.com', '18538753627', '127.0.0.1', '0000-00-00 00:00:00', '2130706433', 1, '1', '0000-00-00 00:00:00', '2017-02-15 14:33:01'),
(3, 2, '2', '779d005fa526b871d424fcab8140582f', '1604583867@qq.com', '18538753627', '127.0.0.1', '0000-00-00 00:00:00', '0', 1, '1', '0000-00-00 00:00:00', '2017-01-17 01:25:23');

-- --------------------------------------------------------

--
-- 表的结构 `ads`
--

CREATE TABLE `ads` (
  `id` int(10) unsigned NOT NULL,
  `position_id` int(11) NOT NULL DEFAULT '0' COMMENT '广告位置',
  `name` text,
  `sub` text,
  `file_id` int(11) NOT NULL DEFAULT '0',
  `url` text,
  `remark` text,
  `rank` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ads`
--

INSERT INTO `ads` (`id`, `position_id`, `name`, `sub`, `file_id`, `url`, `remark`, `rank`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '1', '1210', 108, '19', '1', 0, 1, '2015-11-06 06:58:06', '2017-03-03 03:11:27'),
(2, 2, '2', '', 98, '', '1', 0, 1, '2015-11-06 06:58:20', '2017-03-03 03:11:32'),
(3, 2, '3', '', 99, '', '1', 0, 1, '2015-11-06 06:58:30', '2017-03-03 03:11:35'),
(4, 2, '4', '', 100, '', '1', 333, 1, '2015-11-06 06:58:41', '2017-03-03 01:19:23'),
(6, 2, '广告测试', '2222', 97, '', '1', 222, 1, '2016-01-05 08:14:23', '2017-03-01 09:45:37'),
(7, 2, '111', '111', 95, '11', '111', 222, 1, '2017-03-02 08:35:42', '2017-03-03 03:11:19');

-- --------------------------------------------------------

--
-- 表的结构 `ads_position`
--

CREATE TABLE `ads_position` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `sub` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ads_position`
--

INSERT INTO `ads_position` (`id`, `name`, `sub`, `status`, `created_at`, `updated_at`) VALUES
(1, '幻灯片', '', 1, '2016-12-15 14:42:54', '2017-02-13 02:23:00'),
(2, '首页广告', NULL, 1, '2017-02-17 03:27:55', '0000-00-00 00:00:00'),
(3, '快捷菜单', '', 1, '2017-03-15 07:46:10', '2017-03-15 07:46:10');

-- --------------------------------------------------------

--
-- 表的结构 `analysis`
--

CREATE TABLE `analysis` (
  `id` int(10) unsigned NOT NULL,
  `orders` int(11) NOT NULL DEFAULT '0',
  `trades` float NOT NULL DEFAULT '0',
  `registers` int(11) NOT NULL DEFAULT '0',
  `users` int(11) NOT NULL DEFAULT '0' COMMENT '当天购买人数',
  `date` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `analysis`
--

INSERT INTO `analysis` (`id`, `orders`, `trades`, `registers`, `users`, `date`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 10, 23, '2017-01-03', '2015-11-15 21:57:10', '2016-08-16 15:56:59'),
(2, 3, 46, 12, 3, '2017-01-04', '2015-11-17 14:07:19', '2016-08-16 15:56:50'),
(3, 5, 180, 0, 5, '2016-08-07', '2015-11-18 11:05:35', '2016-08-16 15:56:47'),
(4, 9, 286, 0, 9, '2016-08-08', '2015-11-19 16:08:57', '2016-08-16 15:56:42'),
(5, 1, 36, 0, 1, '2016-08-09', '2015-11-22 15:06:03', '2016-08-16 15:56:37'),
(6, 2, 84, 0, 2, '2016-08-10', '2015-12-22 09:47:38', '2016-08-16 15:56:30'),
(7, 5, 90, 0, 5, '2016-08-11', '2015-12-23 10:49:23', '2016-08-16 15:56:25'),
(8, 65, 72, 0, 0, '2016-08-12', '2016-01-05 20:36:40', '2016-08-16 15:56:18'),
(9, 6, 48, 0, 0, '2016-08-13', '2016-01-06 14:45:39', '2016-08-16 15:56:14'),
(10, 2, 38, 0, 0, '2016-11-26', '2016-01-08 10:07:45', '2016-11-25 15:56:09'),
(11, 1, 24, 0, 0, '2016-11-28', '2016-01-17 11:41:34', '2016-11-27 15:56:04'),
(12, 12, 664, 5, 10, '2016-11-29', '2016-10-31 17:02:27', '2016-11-28 15:56:00'),
(13, 6, 404, 3, 0, '2016-11-30', '2016-08-17 17:02:09', '2016-11-29 11:55:58'),
(14, 0, 0, 0, 1, '2017-01-12', '2017-01-12 01:47:05', '2017-01-12 01:47:05'),
(15, 0, 0, 0, 1, '2017-01-13', '2017-01-12 19:35:11', '2017-01-12 19:35:11'),
(16, 0, 0, 0, 1, '2017-01-14', '2017-01-13 18:37:30', '2017-01-13 18:37:30'),
(17, 0, 0, 1, 1, '2017-01-16', '2017-01-15 18:40:55', '2017-01-16 00:51:10'),
(18, 0, 0, 1, 0, '2017-01-22', '2017-01-21 21:15:32', '2017-01-21 21:15:32'),
(19, 0, 0, 1, 1, '2017-02-08', '2017-02-07 22:37:37', '2017-02-08 05:55:10'),
(20, 0, 0, 0, 1, '2017-02-14', '2017-02-13 23:14:35', '2017-02-13 23:14:35'),
(21, 0, 0, 0, 1, '2017-02-16', '2017-02-15 17:11:59', '2017-02-15 17:11:59'),
(22, 0, 0, 1, 0, '2017-02-18', '2017-02-17 22:32:11', '2017-02-17 22:32:11'),
(23, 0, 0, 0, 1, '2017-02-21', '2017-02-21 03:35:45', '2017-02-21 03:35:45'),
(25, 0, 0, 1, 1, '2017-02-28', '2017-02-27 16:10:41', '2017-02-23 01:01:06'),
(26, 2, 3, 2, 3, '2017-03-01', '2017-02-28 21:12:38', '2017-02-24 02:45:18'),
(28, 5, 36, 0, 0, '2017-03-02', '2017-03-02 09:25:52', '2017-03-02 09:25:52'),
(29, 31, 2062, 0, 0, '2017-03-03', '2017-03-03 03:25:10', '2017-03-03 03:25:10'),
(30, 20, 1106, 4, 4, '2017-03-04', '2017-03-04 01:31:56', '2017-03-04 01:31:56'),
(31, 9, 577, 1, 9, '2017-03-06', '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(32, 50, 204.35, 1, 38, '2017-03-07', '2017-03-07 01:50:13', '2017-03-07 01:50:13'),
(33, 17, 0.17, 3, 5, '2017-03-08', '2017-03-08 01:11:27', '2017-03-08 01:11:27'),
(34, 0, 0, 1, 0, '2017-03-09', '2017-03-09 01:34:16', '2017-03-09 01:34:16'),
(35, 0, 0, 2, 0, '2017-03-10', '2017-03-10 06:30:19', '2017-03-10 06:30:19'),
(36, 0, 0, 1, 0, '2017-03-13', '2017-03-13 08:53:30', '2017-03-13 08:53:30');

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE `article` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `title` text,
  `author` text COMMENT '作者',
  `sub` text,
  `content` text,
  `remark` text,
  `visiter` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`id`, `category_id`, `title`, `author`, `sub`, `content`, `remark`, `visiter`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '关于我们', '大白', '简介', '<p>这里放关于我们内容</p>', '1', 11, 1, '2016-01-05 14:41:14', '2017-03-02 06:52:09');

-- --------------------------------------------------------

--
-- 表的结构 `article_category`
--

CREATE TABLE `article_category` (
  `id` int(11) unsigned NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级',
  `name` text COMMENT '类型',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `remark` text COMMENT '备注',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章类型';

--
-- 转存表中的数据 `article_category`
--

INSERT INTO `article_category` (`id`, `pid`, `name`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 0, '帮助', 1, 'pc端页面下帮助', '2016-12-08 23:17:30', '2017-02-17 02:07:13');

-- --------------------------------------------------------

--
-- 表的结构 `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(8) unsigned NOT NULL,
  `title` char(100) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1:启用0:禁用',
  `rules` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_group`
--

INSERT INTO `auth_group` (`id`, `title`, `status`, `rules`, `created_at`, `updated_at`) VALUES
(1, '超级管理员', 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,', '2017-01-16 01:28:11', '0000-00-00 00:00:00'),
(2, '普通管理员', 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58', '2017-01-16 06:59:52', '2017-02-15 09:08:55');

-- --------------------------------------------------------

--
-- 表的结构 `auth_group_access`
--

CREATE TABLE `auth_group_access` (
  `id` int(10) unsigned NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_group_access`
--

INSERT INTO `auth_group_access` (`id`, `uid`, `group_id`) VALUES
(1, 1, 1),
(3, 2, 1),
(5, 4, 2),
(6, 5, 2),
(7, 6, 2),
(8, 7, 1);

-- --------------------------------------------------------

--
-- 表的结构 `auth_rule`
--

CREATE TABLE `auth_rule` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` char(80) DEFAULT NULL,
  `title` char(20) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_rule`
--

INSERT INTO `auth_rule` (`id`, `name`, `title`, `type`, `rank`, `status`, `condition`, `created_at`, `updated_at`) VALUES
(1, 'admin/index/index', '系统首页', 1, 0, 1, '', '2017-01-09 08:37:37', '0000-00-00 00:00:00'),
(2, 'admin/analysis/user', '用户分析', 1, 0, 1, '', '2017-01-10 07:20:26', '0000-00-00 00:00:00'),
(3, 'admin/analysis/order', '订单分析', 1, 0, 1, '', '2017-01-10 07:25:40', '0000-00-00 00:00:00'),
(4, 'admin/analysis/product', '商品分析', 1, 0, 1, '', '2017-01-10 07:27:28', '0000-00-00 00:00:00'),
(5, 'admin/config.shop/index', '商城设置', 1, 0, 1, '', '2017-01-11 03:13:45', '0000-00-00 00:00:00'),
(6, 'admin/tpl.shop/index', '商城模板设置', 1, 0, 1, '', '2017-01-11 14:52:45', '0000-00-00 00:00:00'),
(7, 'admin/config.pay/index', '支付方式列表', 1, 0, 1, '', '2017-01-11 03:42:28', '0000-00-00 00:00:00'),
(8, 'admin/wx.config/index', '微信设置', 1, 0, 1, '', '2017-01-11 07:10:02', '0000-00-00 00:00:00'),
(9, 'admin/wx.menu/index', '微信菜单设置', 1, 0, 1, '', '2017-01-11 07:39:06', '0000-00-00 00:00:00'),
(10, 'admin/wx.menu/add', '新增修改微信菜单', 1, 0, 1, '', '2017-01-11 08:03:05', '0000-00-00 00:00:00'),
(11, 'admin/wx.menu/del', '删除微信菜单', 1, 0, 1, '', '2017-01-12 03:05:09', '0000-00-00 00:00:00'),
(12, 'admin/wx.reply/index', '微信自定义回复设置', 1, 0, 1, '', '2017-01-12 06:40:51', '0000-00-00 00:00:00'),
(13, 'admin/wx.reply/add', '新增修改自定义回复', 1, 0, 1, '', '2017-01-12 07:19:59', '0000-00-00 00:00:00'),
(14, 'admin/article.category/index', '文章分类', 1, 0, 1, '', '2017-01-14 01:35:20', '0000-00-00 00:00:00'),
(15, 'admin/article.category/add', '新增修改文章分类', 1, 0, 1, '', '2017-01-14 03:30:50', '0000-00-00 00:00:00'),
(16, 'admin/article.category/del', '删除文章分类', 1, 0, 1, '', '2017-01-14 03:42:44', '0000-00-00 00:00:00'),
(17, 'admin/article.index/index', '文章列表', 1, 0, 1, '', '2017-01-14 04:05:56', '0000-00-00 00:00:00'),
(18, 'admin/article.index/add', '新增修改文章', 1, 0, 1, '', '2017-01-16 01:26:29', '0000-00-00 00:00:00'),
(19, 'admin/auth.group/index', '用户组管理', 1, 0, 1, '', '2017-01-16 03:16:56', '0000-00-00 00:00:00'),
(20, 'admin/auth.group/add', '新增修改用户组', 1, 0, 1, '', '2017-01-16 03:44:14', '0000-00-00 00:00:00'),
(21, 'admin/auth.group/del', '删除用户组', 1, 0, 1, '', '2015-10-14 10:18:46', '0000-00-00 00:00:00'),
(22, 'admin/auth.group/update', '更改用户组状态', 1, 0, 1, '', '2015-10-14 10:19:00', '0000-00-00 00:00:00'),
(23, 'admin/auth.admin/index', '管理员管理', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(24, 'admin/auth.admin/add', '新增修改管理员', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(25, 'admin/auth.admin/del', '删除管理员', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(26, 'admin/auth.admin/update', '更改管理员状态', 1, 0, 1, '', '2015-10-14 10:19:56', '0000-00-00 00:00:00'),
(27, 'admin/user.index/index', '用户列表', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(28, 'admin/user.index/add', '新增修改用户', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(29, 'admin/user.index/del', '删除用户', 1, 0, 1, '', '2015-10-14 10:20:12', '0000-00-00 00:00:00'),
(30, 'admin/user.index/update', '更新用户状态', 1, 0, 1, '', '2015-10-14 10:20:18', '0000-00-00 00:00:00'),
(33, 'admin/file/index', '图片列表', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(34, 'admin/file/upload', '图片上传', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(35, 'admin/file/del', '删除图片', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(36, 'admin/shop.product.category/index', '菜单列表', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(37, 'admin/shop.product.category/add', '新增修改菜单', 1, 0, 1, '', '2015-10-18 03:09:21', '0000-00-00 00:00:00'),
(38, 'admin/shop.product.category/del', '删除菜单', 1, 0, 1, '', '2015-10-14 10:21:04', '0000-00-00 00:00:00'),
(39, 'admin/shop.product.label/index', '标签列表', 1, 0, 1, '', '2015-10-14 10:21:11', '0000-00-00 00:00:00'),
(40, 'admin/shop.product.label/add', '新增修改标签', 1, 0, 1, '', '2015-10-14 10:21:17', '0000-00-00 00:00:00'),
(41, 'admin/shop.product.label/del', '删除标签', 1, 0, 1, '', '2015-10-14 10:21:23', '0000-00-00 00:00:00'),
(42, 'admin/shop.product.index/index', '商品列表', 1, 0, 1, '', '2015-10-14 10:21:31', '0000-00-00 00:00:00'),
(43, 'admin/shop.product.index/add', '新增修改商品', 1, 0, 1, '', '2015-10-14 10:21:37', '0000-00-00 00:00:00'),
(44, 'admin/shop.product.index/update', '更新商品状态', 1, 0, 1, '', '2015-10-14 10:21:43', '0000-00-00 00:00:00'),
(45, 'admin/shop.product.index/del', '删除商品', 1, 0, 1, '', '2015-10-14 10:21:48', '0000-00-00 00:00:00'),
(46, 'admin/shop.product.sku/index', 'sku列表', 1, 0, 1, '', '2015-10-14 10:21:54', '0000-00-00 00:00:00'),
(47, 'admin/shop.product.sku/add', '新增修改sku', 1, 0, 1, '', '2015-10-14 10:22:00', '0000-00-00 00:00:00'),
(48, 'admin/shop.product.sku/del', '删除sku', 1, 0, 1, '', '2015-10-14 10:22:09', '0000-00-00 00:00:00'),
(49, 'admin/shop.product.sku/getlist', 'ajax获取sku', 1, 0, 1, '', '2015-10-14 10:22:14', '0000-00-00 00:00:00'),
(50, 'admin/config.wx/delreply', '删除自定义回复', 1, 0, 1, '', '2015-10-18 02:16:38', '0000-00-00 00:00:00'),
(51, 'admin/article.index/del', '删除文章', 1, 0, 1, '', '2015-10-14 06:11:01', '0000-00-00 00:00:00'),
(52, 'admin/shop.product.comment/index', '用户评论', 1, 0, 1, '', '2015-10-18 02:19:17', '0000-00-00 00:00:00'),
(53, 'admin/shop.product.comment/del', '删除评论', 1, 0, 1, '', '2015-10-18 02:19:35', '0000-00-00 00:00:00'),
(54, 'admin/shop.product.comment/update', '更改评论状态', 1, 0, 1, '', '2015-10-14 10:21:23', '0000-00-00 00:00:00'),
(55, 'admin/config.pay/update', '修改支付方式状态', 1, 0, 1, '', '2015-10-18 02:21:13', '0000-00-00 00:00:00'),
(56, 'admin/config.pay/add', '配置支付', 1, 0, 1, '', '2015-10-18 02:24:07', '0000-00-00 00:00:00'),
(57, 'admin/shop.order.index/index', '订单列表', 1, 0, 1, '', '2015-10-14 10:21:23', '0000-00-00 00:00:00'),
(58, 'admin/shop.order.index/detail', '订单详情', 1, 0, 1, '', '2017-01-10 13:52:32', '0000-00-00 00:00:00'),
(59, 'admin/config.delivery/index', '快递列表', 1, 0, 1, '', '2017-01-10 14:00:50', '0000-00-00 00:00:00'),
(60, 'admin/config.delivery/add', '添加修改快递', 1, 0, 1, '', '2017-02-13 18:13:26', '0000-00-00 00:00:00'),
(61, 'admin/config.delivery/del', '删除快递', 1, 0, 1, '', '2017-02-14 23:21:34', '0000-00-00 00:00:00'),
(62, 'admin/config.delivery/update', '启用禁用快递', 1, 0, 1, '', '2017-02-15 01:34:53', '0000-00-00 00:00:00'),
(63, 'admin/shop.order.index/update', '更改订单状态', 1, 0, 1, '', '2017-02-15 03:15:37', '0000-00-00 00:00:00'),
(64, 'admin/shop.product.booking/index', '缺货登记列表', 1, 0, 1, '', '2017-02-15 03:25:32', '0000-00-00 00:00:00'),
(65, 'admin/shop.order.backtype/index', '售后类型列表', 1, 0, 1, '', '2017-02-15 07:15:19', '0000-00-00 00:00:00'),
(66, 'admin/shop.order.backtype/add', '编辑添加售后类型', 1, 0, 1, '', '2017-02-15 07:30:01', '0000-00-00 00:00:00'),
(67, 'admin/shop.order.backtype/del', '删除售后类型', 1, 0, 1, '', '2017-02-15 07:30:41', '0000-00-00 00:00:00'),
(68, 'admin/shop.order.backtype/update', '更新售后类型状态', 1, 0, 1, '', '2017-02-15 07:31:09', '0000-00-00 00:00:00'),
(69, 'admin/shop.order.backlist/index', '售后列表', 1, 0, 1, '', '2017-02-15 08:05:53', '0000-00-00 00:00:00'),
(70, 'admin/shop.order.backlist/update', '更改售后状态', 1, 0, 1, '', '2017-02-15 08:29:54', '0000-00-00 00:00:00'),
(71, 'admin/user.level/index', '会员等级列表', 1, 0, 1, '', '2017-02-15 10:10:19', '0000-00-00 00:00:00'),
(72, 'admin/user.level/add', '新增编辑会员等级', 1, 0, 1, '', '2017-02-15 10:15:43', '0000-00-00 00:00:00'),
(73, 'admin/user.level/del', '删除会员等级', 1, 0, 1, '', '2017-02-15 11:18:56', '0000-00-00 00:00:00'),
(74, 'admin/user.msg/index', '消息列表', 1, 0, 1, '', '2017-02-15 11:19:21', '0000-00-00 00:00:00'),
(75, 'admin/user.msg/add', '添加修改消息', 1, 0, 1, '', '2017-02-15 12:53:51', '0000-00-00 00:00:00'),
(76, 'admin/user.msg/del', '删除消息', 1, 0, 1, '', '2017-02-15 12:54:14', '0000-00-00 00:00:00'),
(77, 'admin/user.msg/update', '更新消息状态', 1, 0, 1, '', '2017-02-15 12:54:38', '0000-00-00 00:00:00'),
(78, 'admin/user.tx/index', '提现列表', 1, 0, 1, '', '2017-02-15 13:17:49', '0000-00-00 00:00:00'),
(79, 'admin/user.tx/update', '更改提现状态', 1, 0, 1, '', '2017-02-15 13:18:23', '0000-00-00 00:00:00'),
(80, 'admin/shop.trade/index', '财务明细', 1, 0, 1, '', '2017-02-15 13:47:10', '0000-00-00 00:00:00'),
(81, 'admin/user.recharge/index', '充值记录', 1, 0, 1, '', '2017-02-15 14:28:04', '0000-00-00 00:00:00'),
(82, 'admin/config.mail/index', '邮件配置', 1, 0, 1, '', '2017-02-16 01:26:09', '0000-00-00 00:00:00'),
(83, 'admin/config.sms/index', '短信配置', 1, 0, 1, '', '2017-02-16 02:22:50', '0000-00-00 00:00:00'),
(84, 'admin/location.country/index', '地址设置', 1, 0, 1, '', '2017-02-16 02:29:01', '0000-00-00 00:00:00'),
(85, 'admin/location.country/add', '新增修改国家', 1, 0, 1, '', '2017-02-16 03:13:01', '0000-00-00 00:00:00'),
(86, 'admin/location.country/update', '更新国家状态', 1, 0, 1, '', '2017-02-16 03:13:26', '0000-00-00 00:00:00'),
(87, 'admin/location.province/index', '省份列表', 1, 0, 1, '', '2017-02-16 03:24:34', '0000-00-00 00:00:00'),
(88, 'admin/location.province/add', '新增修改省份', 1, 0, 1, '', '2017-02-16 03:24:58', '0000-00-00 00:00:00'),
(89, 'admin/location.province/update', '更新省份状态', 1, 0, 1, '', '2017-02-16 03:25:33', '0000-00-00 00:00:00'),
(90, 'admin/location.city/index', '城市列表', 1, 0, 1, '', '2017-02-16 06:13:05', '0000-00-00 00:00:00'),
(92, 'admin/location.city/add', '新增修改城市', 1, 0, 1, '', '2017-02-16 06:13:14', '0000-00-00 00:00:00'),
(93, 'admin/location.city/update', '更新城市状态', 1, 0, 1, '', '2017-02-16 06:13:20', '0000-00-00 00:00:00'),
(94, 'admin/location.district/index', '区域列表', 1, 0, 1, '', '2017-02-16 06:32:17', '0000-00-00 00:00:00'),
(95, 'admin/location.district/add', '新增修改区域', 1, 0, 1, '', '2017-02-16 06:32:20', '0000-00-00 00:00:00'),
(96, 'admin/location.district/update', '更新区域状态', 1, 0, 1, '', '2017-02-16 06:32:24', '0000-00-00 00:00:00'),
(97, 'admin/tpl.mail/index', '邮件模版列表', 1, 0, 1, '', '2017-02-16 07:05:55', '0000-00-00 00:00:00'),
(98, 'admin/tpl.mail/add', '新增修改邮件模版', 1, 0, 1, '', '2017-02-16 07:05:58', '0000-00-00 00:00:00'),
(99, 'admin/tpl.mail/update', '更新邮件模版状态', 1, 0, 1, '', '2017-02-16 07:06:02', '0000-00-00 00:00:00'),
(100, 'admin/tpl.sms/index', '短信模版列表', 1, 0, 1, '', '2017-02-16 07:06:06', '0000-00-00 00:00:00'),
(101, 'admin/tpl.sms/add', '新增修改短信模版', 1, 0, 1, '', '2017-02-16 07:30:03', '0000-00-00 00:00:00'),
(102, 'admin/tpl.sms/update', '开启关闭短信模版', 1, 0, 1, '', '2017-02-16 07:30:08', '0000-00-00 00:00:00'),
(103, 'admin/wx.tplmsg/index', '模版消息列表', 1, 0, 1, '', '2017-02-16 07:30:13', '0000-00-00 00:00:00'),
(104, 'admin/wx.tplmsg/add', '新增编辑模版消息', 1, 0, 1, '', '2017-02-16 10:43:55', '0000-00-00 00:00:00'),
(106, 'admin/wx.tplmsg/update', '开启关闭模版消息', 1, 0, 1, '', '2017-02-16 10:44:03', '0000-00-00 00:00:00'),
(107, 'admin/wx.kefu/index', '多客服设置', 1, 0, 1, '', '2017-02-16 10:44:09', '0000-00-00 00:00:00'),
(108, 'admin/wx.print/index', '微信打印机设置', 1, 0, 1, '', '2017-02-17 01:31:57', '0000-00-00 00:00:00'),
(109, 'admin/article.category/update', '更改文章分类状态', 1, 0, 1, '', '2017-02-17 02:00:42', '0000-00-00 00:00:00'),
(110, 'admin/article.index/update', '更改文章状态', 1, 0, 1, '', '2017-02-17 02:12:54', '0000-00-00 00:00:00'),
(112, 'admin/ads.position/index', '广告位置', 1, 0, 1, '', '2017-02-17 03:11:26', '0000-00-00 00:00:00'),
(113, 'admin/ads.position/update', '开启关闭广告位置', 1, 0, 1, '', '2017-02-17 03:23:45', '0000-00-00 00:00:00'),
(114, 'admin/ads.index/index', '广告列表', 1, 0, 1, '', '2017-02-17 03:31:30', '0000-00-00 00:00:00'),
(115, 'admin/ads.index/add', '新增修改广告', 1, 0, 1, '', '2017-02-17 03:34:48', '0000-00-00 00:00:00'),
(116, 'admin/ads.index/update', '开启关闭广告', 1, 0, 1, '', '2017-02-17 03:35:10', '0000-00-00 00:00:00'),
(117, 'admin/user.index/export', '导出用户', 1, 0, 1, '', '2017-02-17 06:28:31', '0000-00-00 00:00:00'),
(118, 'admin/shop.trade/export', '导出财务明细', 1, 0, 1, '', '2017-02-17 08:35:20', '0000-00-00 00:00:00'),
(119, 'admin/shop.order.index/export', '导出全部订单', 1, 0, 1, '', '2017-02-17 10:14:50', '0000-00-00 00:00:00'),
(120, 'admin/shop.product.index/export', '导出全部商品', 1, 0, 1, '', '2017-02-18 01:11:09', '0000-00-00 00:00:00'),
(121, 'admin/tpl.sms/send', '发送测试短信', 1, 0, 1, '', '2017-02-18 09:08:55', '0000-00-00 00:00:00'),
(122, 'admin/tpl.mail/send', '发送测试邮件', 1, 0, 1, '', '2017-02-18 09:24:27', '0000-00-00 00:00:00'),
(123, 'admin/shop.product.index/sku', '商品sku管理', 1, 0, 1, '', '2017-02-22 14:11:44', '0000-00-00 00:00:00'),
(124, 'admin/addons/index', '插件管理', 1, 0, 1, '', '2017-02-27 01:53:49', '0000-00-00 00:00:00'),
(125, 'admin/addons/shop', '应用商店', 1, 0, 1, '', '2017-03-01 10:09:35', '0000-00-00 00:00:00'),
(126, 'admin/addons/getFileDownload', '下载插件', 1, 0, 1, '', '2017-03-01 10:29:52', '0000-00-00 00:00:00'),
(127, 'admin/addons/compare', '解压插件', 1, 0, 1, '', '2017-03-01 10:30:32', '0000-00-00 00:00:00'),
(128, 'admin/tpl.fee/index', '费用模版', 1, 0, 1, NULL, '2017-03-13 05:46:01', '0000-00-00 00:00:00'),
(129, 'admin/tpl.fee/add', '新增修改费用模版', 1, 0, 1, NULL, '2017-03-13 05:51:57', '0000-00-00 00:00:00'),
(130, 'admin/tpl.fee/update', '更新费用模版', 1, 0, 1, NULL, '2017-03-13 05:53:08', '0000-00-00 00:00:00'),
(131, 'admin/shop.feedback/index', '反馈管理列表', 1, 0, 1, NULL, '2017-03-14 01:06:32', '0000-00-00 00:00:00'),
(132, 'admin/ads.position/add', '新增修改广告位置', 1, 0, 1, NULL, '2017-03-15 07:45:52', '0000-00-00 00:00:00'),
(133, 'admin/base/update', '系统更新', 1, 0, 1, NULL, '2017-03-20 02:57:30', '0000-00-00 00:00:00'),
(134, 'admin/wx.reply/del', '删除自定义回复', 1, 0, 1, NULL, '2017-03-21 02:57:30', '0000-00-00 00:00:00'),
(135, 'admin/wx.robot/index', '图灵机器人', 1, 0, 1, NULL, '2017-03-21 02:57:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `config`
--

CREATE TABLE `config` (
  `id` int(10) unsigned NOT NULL,
  `name` text COMMENT '商城名称',
  `title` text COMMENT '商城标题',
  `description` text COMMENT '商城描述',
  `help` text COMMENT '使用帮助',
  `about` text COMMENT '关于我们',
  `keywords` text COMMENT '商城关键词',
  `welcome` text COMMENT '欢迎信息',
  `logo_id` int(11) NOT NULL DEFAULT '0' COMMENT '商城Logo',
  `qq` varchar(255) DEFAULT NULL COMMENT '客服QQ',
  `tel` text COMMENT '客服电话',
  `mail` text COMMENT '客服邮箱',
  `tx_fee` float NOT NULL DEFAULT '0' COMMENT '提现手续费',
  `theme` text COMMENT '模版主题',
  `debug` int(1) NOT NULL DEFAULT '0' COMMENT '调试模式',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `shop_update` int(1) NOT NULL DEFAULT '1' COMMENT '商城升级',
  `closed_reason` text COMMENT '关站原因',
  `tongji_code` text COMMENT '统计代码',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `config`
--

INSERT INTO `config` (`id`, `name`, `title`, `description`, `help`, `about`, `keywords`, `welcome`, `logo_id`, `qq`, `tel`, `mail`, `tx_fee`, `theme`, `debug`, `status`, `shop_update`, `closed_reason`, `tongji_code`, `created_at`, `updated_at`) VALUES
(1, 'wemall商城', '单用户微商城', '111111', '这里是使用帮助', '', 'wemall', 'wemall欢迎您！\r\n111', 105, '1604583867', '18538753627', '1604583867@qq.com', 0.01, 'default', 0, 1, 1, '商城暂时关闭。。。。', '11111', '2017-01-10 13:30:26', '2017-05-27 01:45:46');

-- --------------------------------------------------------

--
-- 表的结构 `delivery`
--

CREATE TABLE `delivery` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `sub` text,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `delivery`
--

INSERT INTO `delivery` (`id`, `name`, `sub`, `status`, `created_at`, `updated_at`) VALUES
(1, '顺丰快递', '顺丰快递介绍', 0, '2017-02-15 01:14:39', '2017-02-15 01:48:56'),
(2, '中通快递', '', 1, '2017-01-02 19:04:50', '2017-02-02 11:09:02'),
(3, '顺丰速递', '顺丰速递', 0, '2017-01-05 19:59:10', '2017-01-28 10:53:06'),
(4, '顺丰速递', '顺丰速递介绍', 1, '2017-01-05 19:59:28', '2017-02-09 23:24:22'),
(5, '天天快递', '天天快递', 0, '2017-01-07 00:24:51', '2017-02-13 00:54:36'),
(6, '申通速递', '申通速递', 0, '2017-01-12 22:23:46', '2017-02-11 15:45:44'),
(7, '顺丰快递', '顺丰快递', 0, '2017-01-12 22:55:19', '2017-02-01 02:49:04'),
(8, '同城快递', '11111111111111', 0, '2017-03-02 06:46:55', '2017-03-02 06:46:55');

-- --------------------------------------------------------

--
-- 表的结构 `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `feedback`
--

INSERT INTO `feedback` (`id`, `user_id`, `value`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, '很好', NULL, '2016-12-21 04:13:36', '2016-12-21 04:13:36');

-- --------------------------------------------------------

--
-- 表的结构 `fee_tpl`
--

CREATE TABLE `fee_tpl` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `value` float NOT NULL DEFAULT '0' COMMENT '模版费用',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fee_tpl`
--

INSERT INTO `fee_tpl` (`id`, `name`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, '跑腿费', 5, 1, '2017-03-13 05:42:46', '2017-03-13 06:04:31'),
(2, '111', 1, 1, '2017-03-14 02:59:43', '2017-03-14 02:59:43');

-- --------------------------------------------------------

--
-- 表的结构 `file`
--

CREATE TABLE `file` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `ext` text,
  `type` text,
  `savename` text,
  `savepath` text,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `file`
--

INSERT INTO `file` (`id`, `name`, `ext`, `type`, `savename`, `savepath`, `time`) VALUES
(1, '测试图片', 'jpg', 'image/jpeg', 'noimage.gif', 'img/', '2015-10-07 04:24:18'),
(2, '5615e26f54d75.jpg', 'jpg', 'image/jpeg', '563c4f4336719.jpg', '2015-11-06/', '2015-11-06 06:57:07'),
(3, '5615ea4fd93f4.jpg', 'jpg', 'image/jpeg', '563c4f433762a.jpg', '2015-11-06/', '2015-11-06 06:57:07'),
(4, '5615eaa90c582.jpg', 'jpg', 'image/jpeg', '563c4f4337c28.jpg', '2015-11-06/', '2015-11-06 06:57:07'),
(5, '5615eaa90d34c.jpg', 'jpg', 'image/jpeg', '563c4f4338996.jpg', '2015-11-06/', '2015-11-06 06:57:07'),
(6, '5615eaa90dfad.jpg', 'jpg', 'image/jpeg', '563c4f4339b2d.jpg', '2015-11-06/', '2015-11-06 06:57:07'),
(7, '5615eaa90e804.jpg', 'jpg', 'image/jpeg', '563c4f433ab12.jpg', '2015-11-06/', '2015-11-06 06:57:07'),
(9, '1-370x370-1420.jpg', 'jpg', 'image/jpeg', '563c52adac85f.jpg', '2015-11-06/', '2015-11-06 07:11:41'),
(10, '1-370x370-5844-4KHF8KDU.jpg', 'jpg', 'image/jpeg', '563c52bb4b7eb.jpg', '2015-11-06/', '2015-11-06 07:11:55'),
(11, '1-370x370-5985-9KPFBWR1.jpg', 'jpg', 'image/jpeg', '563c52bb4bced.jpg', '2015-11-06/', '2015-11-06 07:11:55'),
(12, '1-370x370-6486-BXPDCPCU.jpg', 'jpg', 'image/jpeg', '563c540523c16.jpg', '2015-11-06/', '2015-11-06 07:17:25'),
(13, '1-370x370-5942-KCHKPX9K.jpg', 'jpg', 'image/jpeg', '563c540524b9c.jpg', '2015-11-06/', '2015-11-06 07:17:25'),
(14, '1-370x370-4394-3YU37TSK.jpg', 'jpg', 'image/jpeg', '563c54052539a.jpg', '2015-11-06/', '2015-11-06 07:17:25'),
(15, '1-370x370-3265-PU41F9AB.jpg', 'jpg', 'image/jpeg', '563c540525aca.jpg', '2015-11-06/', '2015-11-06 07:17:25'),
(16, '1-370x370-4854-4TC46UPX.jpg', 'jpg', 'image/jpeg', '563c5405260d0.jpg', '2015-11-06/', '2015-11-06 07:17:25'),
(17, '1-370x370-6423-YSDU6WA6.jpg', 'jpg', 'image/jpeg', '563c540526487.jpg', '2015-11-06/', '2015-11-06 07:17:25'),
(18, '55fa7cf5d3c70.jpg', 'jpg', 'image/jpeg', '563c61f936dbd.jpg', '2015-11-06/', '2015-11-06 08:16:57'),
(19, '55fa79e11089e.png', 'png', 'image/jpeg', '563c61f937aff.png', '2015-11-06/', '2015-11-06 08:16:57'),
(20, '55fa76b46c708.png', 'png', 'image/jpeg', '563c61f938112.png', '2015-11-06/', '2015-11-06 08:16:57'),
(21, '55fa763dbe297.png', 'png', 'image/jpeg', '563c61f9385ba.png', '2015-11-06/', '2015-11-06 08:16:57'),
(22, '55fa76266b041.png', 'png', 'image/jpeg', '563c61f938931.png', '2015-11-06/', '2015-11-06 08:16:57'),
(23, '55fa759ae7a02.png', 'png', 'image/jpeg', '563c61f938cac.png', '2015-11-06/', '2015-11-06 08:16:57'),
(24, '55fa73efc80f0.png', 'png', 'image/jpeg', '563c61f939289.png', '2015-11-06/', '2015-11-06 08:16:57'),
(25, '55fa737d985f2.png', 'png', 'image/jpeg', '563c61f9395ed.png', '2015-11-06/', '2015-11-06 08:16:57'),
(26, '563885a8a6b84.jpg', 'jpg', 'image/jpeg', '563c61f93985d.jpg', '2015-11-06/', '2015-11-06 08:16:57'),
(27, '1417595621584.jpg', 'jpg', 'image/jpeg', '563c68eff3721.jpg', '2015-11-06/', '2015-11-06 08:46:40'),
(28, '1417597271905.jpg', 'jpg', 'image/jpeg', '563c695de2403.jpg', '2015-11-06/', '2015-11-06 08:48:29'),
(29, '1434268044104.jpg', 'jpg', 'image/jpeg', '563c695de2cbe.jpg', '2015-11-06/', '2015-11-06 08:48:29'),
(66, '56efb341Nda416fd1.jpg', '', 'image/jpeg', '5632a9014bc508accf89a0a64f3b4aa5.jpg', '20170117/', '2017-01-17 07:15:38'),
(67, 'bg_1.jpg', '', 'image/jpeg', 'c81134e3c2458f8df014b3591abf07ca.jpg', '20170208/', '2017-02-08 09:21:08'),
(75, 'd59b165a-fd97-11e6-9041-38c986416005.png', 'png', 'image/jpeg', 'd59b165a-fd97-11e6-9041-38c986416005.png', 'avatar/', '2017-02-28 09:25:24'),
(76, '185dce92-fd98-11e6-bf91-38c986416005.png', 'png', 'image/jpeg', '185dce92-fd98-11e6-bf91-38c986416005.png', 'avatar/', '2017-02-28 09:27:16'),
(77, '4c1d5f2c-fd98-11e6-96a1-38c986416005.png', 'png', 'image/jpeg', '4c1d5f2c-fd98-11e6-96a1-38c986416005.png', 'avatar/', '2017-02-28 09:28:45'),
(78, '9b3bbba8-fd98-11e6-8fc2-38c986416005.png', 'png', 'image/jpeg', '9b3bbba8-fd98-11e6-8fc2-38c986416005.png', 'avatar/', '2017-02-28 09:30:56'),
(79, '315edd9e-fe21-11e6-80e5-38c986416005.png', 'png', 'image/jpeg', '315edd9e-fe21-11e6-80e5-38c986416005.png', 'avatar/', '2017-03-01 01:48:40'),
(80, '01a65d55c313156ac7253f363ff64d.jpg', '', 'image/jpeg', '5447948bab8014fb7c0042241c0e4f57.jpg', '20170301/', '2017-03-01 09:12:17'),
(81, '13R58PICFI9_1024.jpg', '', 'image/jpeg', '097fd1fda978d094f9904af0608f221d.jpg', '20170301/', '2017-03-01 09:12:24'),
(82, '36B58PICNjS_1024.jpg', '', 'image/jpeg', '84a482526055161c8f0bbd5adb187cea.jpg', '20170301/', '2017-03-01 09:12:28'),
(83, '58dd09156201b1902851409b3e83cd24.jpg', '', 'image/jpeg', 'fee7be02ebd9ff2230d27e5f761d0f84.jpg', '20170301/', '2017-03-01 09:12:33'),
(84, '0179ae57a457600000018c1ba38fc9.jpg', '', 'image/jpeg', '0a95e34df473253874107f3c24a750de.jpg', '20170301/', '2017-03-01 09:12:37'),
(85, '8326cffc1e178a82a8cfbbfdf603738da877e8f8.jpg', '', 'image/jpeg', 'ec63d8efbfd68e95e8511b23f14d0946.jpg', '20170301/', '2017-03-01 09:12:50'),
(86, 'bf76bdfbcd6ef583ba30b161e85e57dc.jpg', '', 'image/jpeg', '20bbc27d4764d0d9563b64e8759e648e.jpg', '20170301/', '2017-03-01 09:12:56'),
(87, 'u=176602520,3100091901&fm=23&gp=0.jpg', '', 'image/jpeg', '4c9fbc348b5f3523cb9f036d0fe082e1.jpg', '20170301/', '2017-03-01 09:13:00'),
(88, 'u=2457965475,4177702945&fm=23&gp=0.jpg', '', 'image/jpeg', '67d75c663a18fd76260a0ffa7b0478a7.jpg', '20170301/', '2017-03-01 09:13:04'),
(89, '4d6d5a0e-fe5f-11e6-b32a-38c986416005.png', 'png', 'image/jpeg', '4d6d5a0e-fe5f-11e6-b32a-38c986416005.png', 'avatar/', '2017-03-01 09:13:20'),
(90, '8326cffc1e178a82a8cfbbfdf603738da877e8f8.jpg', '', 'image/jpeg', 'dd6186b0f03df315719dd86b5ddeff44.jpg', '20170301/', '2017-03-01 09:25:56'),
(91, '9777035_165424741000_2.jpg', '', 'image/jpeg', '1868123d999a98b3baf9366d003e0b0f.jpg', '20170301/', '2017-03-01 09:26:01'),
(92, '0_xuemanni_6513_20141201142805.jpg', '', 'image/jpeg', '8733de38978e3aeda8d602d83fad15a0.jpg', '20170301/', '2017-03-01 09:26:15'),
(93, '013a2c57a4554a0000018c1bf33dc2.jpg', '', 'image/jpeg', '500ab48dd025e0d0c9e1403c89d15f3a.jpg', '20170301/', '2017-03-01 09:26:22'),
(94, 'u=2457965475,4177702945&fm=23&gp=0.jpg', '', 'image/jpeg', '01efc214d75aecd27f013f7b8a875701.jpg', '20170301/', '2017-03-01 09:26:34'),
(95, '58b5146dNad045c4c.jpg', '', 'image/jpeg', '94e731c90910a50acea3866a476e9688.jpg', '20170301/', '2017-03-01 09:40:34'),
(96, '4_rrgy.jpg', '', 'image/jpeg', 'edcc54a680e487ff1dd558609751626a.jpg', '20170301/', '2017-03-01 09:43:37'),
(97, '750_yzht-360_570x273_90.jpg', '', 'image/jpeg', '73b89408e0c0e7f88b704388b179bf86.jpg', '20170301/', '2017-03-01 09:45:34'),
(98, '1487647018790_570x273_90.jpg', '', 'image/jpeg', 'dd64da07f97009638a07f0194b4f4712.jpg', '20170301/', '2017-03-01 09:45:47'),
(99, '1487670798030_570x273_90.jpg', '', 'image/jpeg', 'dc27aef302cff30e0630d673ed528009.jpg', '20170301/', '2017-03-01 09:46:26'),
(100, '1487661952019_570x273_90.jpg', '', 'image/jpeg', '0a9e06f5c08b5ee0129167980de3f721.jpg', '20170301/', '2017-03-01 09:47:09'),
(101, '8316119c-feef-11e6-8cc9-38c986416005.png', 'png', 'image/jpeg', '8316119c-feef-11e6-8cc9-38c986416005.png', 'avatar/', '2017-03-02 02:25:41'),
(102, '37acf652-fef0-11e6-985c-38c986416005.png', 'png', 'image/jpeg', '37acf652-fef0-11e6-985c-38c986416005.png', 'avatar/', '2017-03-02 02:30:36'),
(103, '8cfc870c-fef1-11e6-9146-38c986416005.png', 'png', 'image/jpeg', '8cfc870c-fef1-11e6-9146-38c986416005.png', 'avatar/', '2017-03-02 02:40:14'),
(104, 'd2b2275ac43cc8bce03760c162d23724ca67b803122af-qLuXr8_fw658.jpeg', '', 'image/jpeg', 'a02795d30768a851b4a90554698ce2d2.jpeg', '20170302/', '2017-03-02 07:15:26'),
(105, 'wemall.png', '', 'image/png', '718dd9bbdbd36a7f8f2654dcfebb03cb.png', '20170302/', '2017-03-02 07:15:53'),
(106, '0213.jpg', '', 'image/jpeg', '8c99ecd017cb478f5f3400a76eb729ea.jpg', '20170302/', '2017-03-02 08:27:57'),
(107, '5_02135.jpg', '', 'image/jpeg', '96b2251cecc3cc0731b8fd585b2ef104.jpg', '20170302/', '2017-03-02 08:30:04'),
(108, '1_022702.jpg', '', 'image/jpeg', '5e9ba5046a0b588bdfafa1ffd59add44.jpg', '20170302/', '2017-03-02 08:31:40'),
(109, 'longchamp_crossbody_bags_roseau_sakura_1016873A26_0.png', '', 'image/png', '280e56d761a25c625257891807ee2256.png', '20170302/', '2017-03-02 09:23:28'),
(110, '58b56445N7a3876dc.jpeg', '', 'image/jpeg', 'b8eef843324e0ba721c3781b4924dfd9.jpeg', '20170302/', '2017-03-02 09:55:32'),
(111, '58abb2dbN2d71963d.jpg', '', 'image/jpeg', '5b6ab452593c5e7ce80a9245866be5d8.jpg', '20170302/', '2017-03-02 10:26:26'),
(112, '58abb1eaN847f21e1.jpg', '', 'image/jpeg', '0f9793f82399770d55ca184f8a6782cb.jpg', '20170302/', '2017-03-02 10:27:54'),
(113, '36388a12-00ac-11e7-aa6d-38c986416005.png', 'png', 'image/jpeg', '36388a12-00ac-11e7-aa6d-38c986416005.png', 'avatar/', '2017-03-04 07:28:51'),
(114, 'c7fdca30-00ba-11e7-9859-38c986416005.png', 'png', 'image/jpeg', 'c7fdca30-00ba-11e7-9859-38c986416005.png', 'avatar/', '2017-03-04 09:13:09'),
(115, 'c7fdcf94-00ba-11e7-b37a-38c986416005.png', 'png', 'image/jpeg', 'c7fdcf94-00ba-11e7-b37a-38c986416005.png', 'avatar/', '2017-03-04 09:13:09'),
(116, 'c7fdca62-00ba-11e7-bbfe-38c986416005.png', 'png', 'image/jpeg', 'c7fdca62-00ba-11e7-bbfe-38c986416005.png', 'avatar/', '2017-03-04 09:13:09'),
(117, 'TB25L4.c3SI.eBjy1XcXXc1jXXa_!!2597188887.jpg', '', 'image/jpeg', 'ee93143bfc28e9eaceaa525b812ddf53.jpg', '20170304/', '2017-03-04 09:31:01'),
(118, 'TB2jZYwa5pnpuFjSZFIXXXh2VXa_!!2041961913.jpg_400x400.jpg', '', 'image/jpeg', '1728f7e25c847a2cfd09fa3c6bd7aa88.jpg', '20170304/', '2017-03-04 09:36:02'),
(119, 'c3ef5450-031b-11e7-b19d-38c986416005.png', 'png', 'image/jpeg', 'c3ef5450-031b-11e7-b19d-38c986416005.png', 'avatar/', '2017-03-07 09:52:42'),
(120, 'c1d27f3e-03a8-11e7-a6f3-38c986416005.png', 'png', 'image/jpeg', 'c1d27f3e-03a8-11e7-a6f3-38c986416005.png', 'avatar/', '2017-03-08 02:41:45'),
(121, '840ad852-0468-11e7-b144-38c986416005.png', 'png', 'image/jpeg', '840ad852-0468-11e7-b144-38c986416005.png', 'avatar/', '2017-03-09 01:34:20'),
(122, '89614146-07ca-11e7-8da9-38c986416005.png', 'png', 'image/jpeg', '89614146-07ca-11e7-8da9-38c986416005.png', 'avatar/', '2017-03-13 08:53:37'),
(123, '611cbbf4-0863-11e7-8c5a-38c986416005.png', 'png', 'image/jpeg', '611cbbf4-0863-11e7-8c5a-38c986416005.png', 'avatar/', '2017-03-14 03:07:43'),
(124, '748ed74e-0863-11e7-b553-38c986416005.png', 'png', 'image/jpeg', '748ed74e-0863-11e7-b553-38c986416005.png', 'avatar/', '2017-03-14 03:08:11'),
(125, 'a55102f8-0863-11e7-ad11-38c986416005.png', 'png', 'image/jpeg', 'a55102f8-0863-11e7-ad11-38c986416005.png', 'avatar/', '2017-03-14 03:09:32'),
(126, '452cd0da-08a1-11e7-8be7-38c986416005.png', 'png', 'image/jpeg', '452cd0da-08a1-11e7-8be7-38c986416005.png', 'avatar/', '2017-03-14 10:30:41'),
(127, '47281a70-08a1-11e7-ad7e-38c986416005.png', 'png', 'image/jpeg', '47281a70-08a1-11e7-ad7e-38c986416005.png', 'avatar/', '2017-03-14 10:30:43'),
(128, '4797f6a6-08a1-11e7-b16c-38c986416005.png', 'png', 'image/jpeg', '4797f6a6-08a1-11e7-b16c-38c986416005.png', 'avatar/', '2017-03-14 10:30:44'),
(129, '47ef3dbc-08a1-11e7-b740-38c986416005.png', 'png', 'image/jpeg', '47ef3dbc-08a1-11e7-b740-38c986416005.png', 'avatar/', '2017-03-14 10:30:44'),
(130, '48a08860-08a1-11e7-80e9-38c986416005.png', 'png', 'image/jpeg', '48a08860-08a1-11e7-80e9-38c986416005.png', 'avatar/', '2017-03-14 10:30:46'),
(131, 'd7d435f8-0929-11e7-82db-38c986416005.png', 'png', 'image/jpeg', 'd7d435f8-0929-11e7-82db-38c986416005.png', 'avatar/', '2017-03-15 02:48:19'),
(132, 'e78a304c-0929-11e7-8730-38c986416005.png', 'png', 'image/jpeg', 'e78a304c-0929-11e7-8730-38c986416005.png', 'avatar/', '2017-03-15 02:48:44'),
(133, 'f089bffa-0929-11e7-9cdf-38c986416005.png', 'png', 'image/jpeg', 'f089bffa-0929-11e7-9cdf-38c986416005.png', 'avatar/', '2017-03-15 02:48:59'),
(134, 'f3c70a2e-0929-11e7-a7af-38c986416005.png', 'png', 'image/jpeg', 'f3c70a2e-0929-11e7-a7af-38c986416005.png', 'avatar/', '2017-03-15 02:49:04'),
(135, '0bc4b288-092b-11e7-8a93-38c986416005.png', 'png', 'image/jpeg', '0bc4b288-092b-11e7-8a93-38c986416005.png', 'avatar/', '2017-03-15 02:56:59');

-- --------------------------------------------------------

--
-- 表的结构 `location`
--

CREATE TABLE `location` (
  `id` smallint(5) unsigned NOT NULL,
  `pid` smallint(5) NOT NULL DEFAULT '0',
  `name` varchar(120) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:国家，1:省份，2:市，3:区',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `location`
--

INSERT INTO `location` (`id`, `pid`, `name`, `type`, `status`) VALUES
(1, 0, '中国', 0, 0),
(2, 1, '河南', 1, 0),
(3, 2, '郑州', 2, 0),
(4, 2, '开封', 2, 0),
(5, 3, '金水区', 3, 0);

-- --------------------------------------------------------

--
-- 表的结构 `mail`
--

CREATE TABLE `mail` (
  `id` int(11) unsigned NOT NULL,
  `host` text COMMENT '服务器地址',
  `port` int(11) DEFAULT NULL COMMENT '服务器端口',
  `secure` double NOT NULL DEFAULT '0' COMMENT '1:加密0:不加密',
  `replyTo` text COMMENT '回信地址',
  `user` text COMMENT '发送邮箱',
  `pass` text COMMENT '授权码,通过QQ获取',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `mail`
--

INSERT INTO `mail` (`id`, `host`, `port`, `secure`, `replyTo`, `user`, `pass`, `created_at`, `updated_at`) VALUES
(1, 'smtpdm.aliyun.com', 465, 1, 'koahub@163.com', 'mail@notice.koahub.com', '786699892smtp', '2017-02-16 01:52:41', '2017-03-02 06:48:29');

-- --------------------------------------------------------

--
-- 表的结构 `mail_tpl`
--

CREATE TABLE `mail_tpl` (
  `id` int(11) unsigned NOT NULL,
  `type` text COMMENT '类型',
  `name` text COMMENT '模版名',
  `content` text COMMENT '内容',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态1:开启0:关闭',
  `mail` text COMMENT '测试发送邮箱',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mail_tpl`
--

INSERT INTO `mail_tpl` (`id`, `type`, `name`, `content`, `status`, `mail`, `created_at`, `updated_at`) VALUES
(1, 'register', '注册模版', '<p>您好，欢迎您注册wemallshop微信商城，您的验证码是：$code</p>', 1, '1604583867@qq.com', '0000-00-00 00:00:00', '2017-02-18 09:38:44');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_applet`
--

CREATE TABLE `oauth_applet` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `openid` text COMMENT '用户的标识，对当前小程序唯一',
  `nickname` text COMMENT '用户的昵称',
  `gender` int(11) NOT NULL DEFAULT '0' COMMENT '1:男2:女0:未知',
  `city` text COMMENT '用户所在城市',
  `province` text COMMENT '用户所在省份',
  `language` text COMMENT '语言',
  `avatarUrl` text COMMENT '用户头像',
  `unionid` text COMMENT '统一用户',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信小程序登陆';

--
-- 转存表中的数据 `oauth_applet`
--

INSERT INTO `oauth_applet` (`id`, `user_id`, `openid`, `nickname`, `gender`, `city`, `province`, `language`, `avatarUrl`, `unionid`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', 0, '', '', '', '', '', '2017-03-13 13:24:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_wx`
--

CREATE TABLE `oauth_wx` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `openid` text COMMENT '用户的标识，对当前公众号唯一',
  `nickname` text COMMENT '用户的昵称',
  `sex` int(11) NOT NULL DEFAULT '0' COMMENT '1:男2:女0:未知',
  `city` text COMMENT '用户所在城市',
  `country` text COMMENT '用户所在国家',
  `province` text COMMENT '用户所在省份',
  `language` text COMMENT '语言',
  `headimgurl` text COMMENT '用户头像',
  `subscribe_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '关注时间',
  `unionid` text COMMENT '统一用户',
  `subscribe` int(11) NOT NULL DEFAULT '1' COMMENT '1:关注0:取消',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信登陆';

--
-- 转存表中的数据 `oauth_wx`
--

INSERT INTO `oauth_wx` (`id`, `user_id`, `openid`, `nickname`, `sex`, `city`, `country`, `province`, `language`, `headimgurl`, `subscribe_time`, `unionid`, `subscribe`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', 0, '', '', '', '', '', '0000-00-00 00:00:00', '', 0, '2017-03-13 13:24:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `order`
--

CREATE TABLE `order` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `delivery_id` int(11) NOT NULL DEFAULT '0' COMMENT '快递方式',
  `delivery_code` text COMMENT '快递单号',
  `delivery_time` text,
  `orderid` text COMMENT '订单号',
  `totalprice` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `totalprice_org` float NOT NULL DEFAULT '0' COMMENT '原价',
  `payment_id` int(11) NOT NULL DEFAULT '0' COMMENT '付款方式',
  `pay_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:已支付0:未支付',
  `coupon` text COMMENT '优惠券',
  `freight` float NOT NULL DEFAULT '0',
  `discount` int(11) NOT NULL DEFAULT '0',
  `totalscore` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1:到店自提0:送货上门',
  `stores` text COMMENT '自提门店',
  `remark` text COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:待发货，1:已发货，2:已完成，3:已评价, -1:已关闭，-2:待退款，-3:已退款',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `order`
--

INSERT INTO `order` (`id`, `user_id`, `delivery_id`, `delivery_code`, `delivery_time`, `orderid`, `totalprice`, `totalprice_org`, `payment_id`, `pay_status`, `coupon`, `freight`, `discount`, `totalscore`, `employee_id`, `type`, `stores`, `remark`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '', '10:30-11:30', '1703131545409', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 1, '', NULL, -1, '2017-03-03 09:44:40', '2017-03-03 09:44:40'),
(2, 1, 0, '', '10:30-11:30', '1703030547309', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":"18538553590"}', NULL, -1, '2017-03-03 09:47:30', '2017-03-03 09:47:30'),
(3, 1, 0, '', '10:30-11:30', '1703030604472', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:04:47', '2017-03-03 10:04:47'),
(4, 1, 0, '', '10:30-11:30', '1703030618553', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:18:55', '2017-03-03 10:18:55'),
(5, 1, 0, '', '10:30-11:30', '1703030622401', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:22:39', '2017-03-03 10:22:39'),
(6, 1, 0, '', '10:30-11:30', '1703030627455', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:27:45', '2017-03-03 10:27:45'),
(7, 1, 0, '', '10:30-11:30', '1703030631071', 100.00, 0, 4, 0, '', 0, 0, 11, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:31:07', '2017-03-03 10:31:07'),
(8, 1, 0, '', '10:30-11:30', '1703030631505', 0.00, 0, 4, 1, '', 0, 0, 0, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:31:50', '2017-03-03 10:31:50'),
(9, 1, 0, '', '10:30-11:30', '1703030638338', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:38:33', '2017-03-03 10:38:33'),
(10, 1, 0, '', '10:30-11:30', '1703030640178', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:40:17', '2017-03-03 10:40:17'),
(11, 1, 0, '', '10:30-11:30', '1703030641204', 100.00, 0, 4, 0, '', 0, 0, 11, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:41:20', '2017-03-03 10:41:20'),
(12, 1, 0, '', '10:30-11:30', '1703030641572', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:41:57', '2017-03-03 10:41:57'),
(13, 1, 0, '', '10:30-11:30', '1703030643257', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:43:25', '2017-03-03 10:43:25'),
(14, 1, 0, '', '10:30-11:30', '1703030645476', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:45:47', '2017-03-03 10:45:47'),
(15, 1, 0, '', '10:30-11:30', '1703030648109', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:48:10', '2017-03-03 10:48:10'),
(16, 1, 0, '', '10:30-11:30', '1703030652376', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:52:36', '2017-03-03 10:52:36'),
(17, 1, 0, '', '10:30-11:30', '1703030654262', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:54:26', '2017-03-03 10:54:26'),
(18, 1, 0, '', '10:30-11:30', '1703030657114', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 10:57:10', '2017-03-03 10:57:10'),
(19, 1, 0, '', '10:30-11:30', '1703030701407', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, 0, '2017-03-03 11:01:40', '2017-03-03 11:01:40'),
(20, 1, 0, '', '10:30-11:30', '1703030702585', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, -1, '2017-03-03 11:02:58', '2017-03-03 11:02:58'),
(21, 1, 0, '', '10:30-11:30', '1703030706282', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 1, '{"address":0,"phone":""}', NULL, -1, '2017-03-03 11:06:26', '2017-03-03 11:06:26'),
(22, 1, 0, '', '10:30-11:30', '1703030710015', 800.00, 0, 1, 0, '', 0, 0, 88, 0, 1, '{"address":0,"phone":""}', NULL, -1, '2017-03-03 11:10:01', '2017-03-03 11:10:01'),
(23, 1, 0, '', '10:30-11:30', '1703030718412', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":""}', NULL, -1, '2017-03-03 11:18:41', '2017-03-03 11:18:41'),
(24, 1, 0, '', '10:30-11:30', '1703040931564', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":0,"phone":""}', NULL, -1, '2017-03-04 01:31:56', '2017-03-04 01:31:56'),
(25, 1, 0, '', '10:30-11:30', '1703040934395', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":"18538553590"}', NULL, -1, '2017-03-04 01:34:38', '2017-03-04 01:34:38'),
(26, 1, 0, '', '10:30-11:30', '1703040938524', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":0,"phone":""}', NULL, -1, '2017-03-04 01:38:52', '2017-03-04 01:38:52'),
(27, 1, 0, '', '10:30-11:30', '1703040939391', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":"185385535960"}', NULL, -1, '2017-03-04 01:39:39', '2017-03-04 01:39:39'),
(28, 1, 0, '', '10:30-11:30', '1703040948436', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 01:48:43', '2017-03-04 01:48:43'),
(29, 1, 0, '', '10:30-11:30', '1703040202519', 100.00, 0, 1, 0, '', 0, 0, 11, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":""}', NULL, 0, '2017-03-04 06:02:50', '2017-03-04 06:02:50'),
(30, 1, 0, '', '14:30-15:30', '1703040235347', 312.00, 0, 1, 0, '', 0, 0, 45, 0, 0, '{"address":0,"phone":""}', NULL, -1, '2017-03-04 06:35:33', '2017-03-04 06:35:33'),
(31, 1, 0, '', '14:30-15:30', '1703040325461', 36.00, 0, 1, 0, '', 0, 0, 36, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 07:25:46', '2017-03-04 07:25:46'),
(32, 1, 0, '', '14:30-15:30', '1703040327041', 36.00, 0, 1, 0, '', 0, 0, 36, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 07:27:04', '2017-03-04 07:27:04'),
(33, 26, 0, '', '14:30-15:30', '1703040331247', 36.00, 0, 1, 0, '', 0, 0, 36, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 07:31:24', '2017-03-04 07:31:24'),
(34, 26, 0, '', '10:30-11:30', '1703040333008', 12.00, 0, 1, 0, '', 0, 0, 12, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 07:33:00', '2017-03-04 07:33:00'),
(35, 24, 0, '', '14:30-15:30', '1703040342277', 24.00, 0, 1, 0, '', 0, 0, 24, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 07:42:27', '2017-03-04 07:42:27'),
(36, 20, 0, '', '14:30-15:30', '1703040540573', 12.00, 0, 4, 0, '', 0, 0, 12, 0, 0, '{"address":0,"phone":""}', NULL, 0, '2017-03-04 09:40:56', '2017-03-04 09:40:56'),
(37, 1, 0, '', '10:30-11:30', '1703040600401', 12.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:00:40', '2017-03-04 10:00:40'),
(38, 1, 0, '', '10:30-11:30', '1703040603339', 12.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:03:33', '2017-03-04 10:03:33'),
(39, 1, 0, '', '10:30-11:30', '1703040607469', 12.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:07:46', '2017-03-04 10:07:46'),
(40, 1, 0, '', '10:30-11:30', '1703040608335', 0.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:08:33', '2017-03-04 10:08:33'),
(41, 1, 0, '', '10:30-11:30', '1703040610138', 0.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:10:13', '2017-03-04 10:10:13'),
(42, 1, 0, '', '10:30-11:30', '1703040612194', 2.00, 12, 1, 0, '{"code":"933758","price":10}', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:12:19', '2017-03-04 10:12:19'),
(43, 1, 0, '', '10:30-11:30', '1703040647593', 0.00, 12, 1, 0, '{"code":"118775","price":20}', 0, 0, 12, 0, 0, '', NULL, 0, '2017-03-04 10:47:58', '2017-03-04 10:47:58'),
(44, 23, 0, '', '10:30-11:30', '1703060219211', 136.00, 136, 2, 0, '', 0, 0, 46, 0, 0, '', NULL, 0, '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(45, 23, 0, '', '15:30-17:30', '1703060242355', 48.00, 48, 3, 0, '', 0, 0, 48, 0, 0, '', NULL, 0, '2017-03-06 06:42:35', '2017-03-06 06:42:35'),
(46, 23, 0, '', '15:30-17:30', '1703060248146', 72.00, 72, 1, 0, '', 0, 0, 72, 0, 0, '', NULL, 0, '2017-03-06 06:48:14', '2017-03-06 06:48:14'),
(47, 23, 0, '', '15:30-17:30', '1703060251125', 48.00, 48, 1, 0, '', 0, 0, 48, 0, 0, '', NULL, 0, '2017-03-06 06:51:12', '2017-03-06 06:51:12'),
(48, 23, 0, '', '10:30-11:30', '1703060254531', 36.00, 36, 1, 0, '', 0, 0, 36, 0, 0, '', NULL, 0, '2017-03-06 06:54:52', '2017-03-06 06:54:52'),
(49, 23, 0, '', '15:30-17:30', '1703060300539', 129.00, 129, 1, 0, '', 0, 0, 100, 0, 0, '', NULL, 0, '2017-03-06 07:00:53', '2017-03-06 07:00:53'),
(50, 23, 0, '', '15:30-17:30', '1703060322038', 36.00, 36, 1, 0, '', 0, 0, 36, 0, 0, '', NULL, 0, '2017-03-06 07:22:03', '2017-03-06 07:22:03'),
(51, 23, 0, '', '10:30-11:30', '1703060334341', 36.00, 36, 1, 0, '', 0, 0, 36, 0, 0, '', NULL, 0, '2017-03-06 07:34:34', '2017-03-06 07:34:34'),
(52, 23, 0, '', '14:30-15:30', '1703060622108', 36.00, 36, 1, 0, '', 0, 0, 36, 0, 0, '', '', 0, '2017-03-06 10:22:10', '2017-03-06 10:22:10'),
(53, 24, 0, '', '14:30-15:30', '1703070950141', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', -1, '2017-03-07 01:50:13', '2017-03-07 01:50:13'),
(54, 1, 0, '', '10:30-11:30', '1703071005121', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 02:05:12', '2017-03-07 02:05:12'),
(55, 1, 0, '', '10:30-11:30', '1703071005467', 12.00, 12, 3, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 02:05:46', '2017-03-07 02:05:46'),
(56, 1, 0, '', '10:30-11:30', '1703071006315', 12.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 02:06:31', '2017-03-07 02:06:31'),
(57, 1, 0, '', '10:30-11:30', '1703071008512', 12.00, 12, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 02:08:50', '2017-03-07 02:08:50'),
(58, 23, 0, '', '14:30-15:30', '1703071054049', 0.07, 0.07, 1, 0, '', 0, 0, 84, 0, 0, '', '', 0, '2017-03-07 02:54:04', '2017-03-07 02:54:04'),
(59, 24, 0, '', '14:30-15:30', '1703071103526', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 03:03:51', '2017-03-07 03:03:51'),
(60, 20, 0, '', '14:30-15:30', '1703071110469', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '111', 0, '2017-03-07 03:10:46', '2017-03-07 03:10:46'),
(61, 24, 0, '', '14:30-15:30', '1703071151155', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', 'aa', 0, '2017-03-07 03:51:15', '2017-03-07 03:51:15'),
(62, 24, 0, '', '14:30-15:30', '1703071155302', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 03:55:30', '2017-03-07 03:55:30'),
(63, 24, 0, '', '14:30-15:30', '1703071205012', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 04:05:01', '2017-03-07 04:05:01'),
(64, 24, 0, '', '14:30-15:30', '1703070226464', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 06:26:45', '2017-03-07 06:26:45'),
(65, 1, 0, '', '10:30-11:30', '1703070236293', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 06:36:29', '2017-03-07 06:36:29'),
(66, 24, 0, '', '14:30-15:30', '1703070303478', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:03:47', '2017-03-07 07:03:47'),
(67, 24, 0, '', '14:30-15:30', '1703070315583', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:15:58', '2017-03-07 07:15:58'),
(68, 24, 0, '', '14:30-15:30', '1703070322458', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:22:45', '2017-03-07 07:22:45'),
(69, 1, 0, '', '10:30-11:30', '1703070325553', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:25:55', '2017-03-07 07:25:55'),
(70, 20, 0, '', '14:30-15:30', '1703070326332', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:26:32', '2017-03-07 07:26:32'),
(71, 1, 0, '', '10:30-11:30', '1703070327224', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:27:22', '2017-03-07 07:27:22'),
(72, 1, 0, '', '10:30-11:30', '1703070328159', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:28:15', '2017-03-07 07:28:15'),
(73, 1, 0, '', '10:30-11:30', '1703070329242', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:29:24', '2017-03-07 07:29:24'),
(74, 20, 0, '', '14:30-15:30', '1703070329379', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:29:36', '2017-03-07 07:29:36'),
(75, 1, 0, '', '10:30-11:30', '1703070330066', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:30:06', '2017-03-07 07:30:06'),
(76, 1, 0, '', '10:30-11:30', '1703070331266', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 2, '2017-03-07 07:31:26', '2017-03-07 07:31:26'),
(77, 20, 0, '', '14:30-15:30', '1703070331296', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:31:29', '2017-03-07 07:31:29'),
(78, 1, 0, '', '10:30-11:30', '1703070332019', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:32:01', '2017-03-07 07:32:01'),
(79, 20, 0, '', '14:30-15:30', '1703070334409', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:34:40', '2017-03-07 07:34:40'),
(80, 20, 0, '', '14:30-15:30', '1703070337232', 12.00, 12, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:37:23', '2017-03-07 07:37:23'),
(81, 20, 0, '', '14:30-15:30', '1703070348015', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:48:01', '2017-03-07 07:48:01'),
(82, 21, 0, '', '10:30-11:30', '1703070350517', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:50:51', '2017-03-07 07:50:51'),
(83, 21, 0, '', '10:30-11:30', '1703070353431', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:53:43', '2017-03-07 07:53:43'),
(84, 21, 0, '', '10:30-11:30', '1703070359569', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 07:59:56', '2017-03-07 07:59:56'),
(85, 21, 0, '', '10:30-11:30', '1703070401248', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 08:01:24', '2017-03-07 08:01:24'),
(86, 20, 0, '', '14:30-15:30', '1703070409036', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 08:09:03', '2017-03-07 08:09:03'),
(87, 20, 0, '', '14:30-15:30', '1703070417546', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 08:17:54', '2017-03-07 08:17:54'),
(88, 20, 0, '', '14:30-15:30', '1703070421347', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 08:21:34', '2017-03-07 08:21:34'),
(89, 20, 0, '', '14:30-15:30', '1703070427241', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 08:27:24', '2017-03-07 08:27:24'),
(90, 21, 0, '', '10:30-11:30', '1703070512087', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 09:12:08', '2017-03-07 09:12:08'),
(91, 21, 0, '', '10:30-11:30', '1703070524248', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 09:24:24', '2017-03-07 09:24:24'),
(92, 21, 0, '', '10:30-11:30', '1703070557328', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 09:57:31', '2017-03-07 09:57:31'),
(93, 21, 0, '', '10:30-11:30', '1703070605101', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 10:05:10', '2017-03-07 10:05:10'),
(94, 21, 0, '', '10:30-11:30', '1703070623401', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":"18538553590"}', '', 0, '2017-03-07 10:23:40', '2017-03-07 10:23:40'),
(95, 21, 0, '', '10:30-11:30', '1703070626013', 0.01, 0.01, 4, 0, '', 0, 0, 12, 0, 1, '{"address":"郑州市金水区北三环园田路11号","phone":"18538553590"}', '', -1, '2017-03-07 10:26:01', '2017-03-07 10:26:01'),
(96, 23, 0, '', '15:30-17:30', '1703070644487', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 10:44:47', '2017-03-07 10:44:47'),
(97, 23, 0, '', '15:30-17:30', '1703070645215', 0.00, 0, 2, 0, '', 0, 0, 0, 0, 0, '', '', 0, '2017-03-07 10:45:21', '2017-03-07 10:45:21'),
(98, 23, 0, '', '15:30-17:30', '1703070646087', 0.00, 0, 2, 0, '', 0, 0, 0, 0, 0, '', '', 0, '2017-03-07 10:46:08', '2017-03-07 10:46:08'),
(99, 23, 0, '', '15:30-17:30', '1703070652256', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 10:52:25', '2017-03-07 10:52:25'),
(100, 23, 0, '', '15:30-17:30', '1703070652424', 0.00, 0, 2, 0, '', 0, 0, 0, 0, 0, '', '', 0, '2017-03-07 10:52:42', '2017-03-07 10:52:42'),
(101, 23, 0, '', '15:30-17:30', '1703070652468', 0.00, 0, 2, 0, '', 0, 0, 0, 0, 0, '', '', 0, '2017-03-07 10:52:46', '2017-03-07 10:52:46'),
(102, 23, 0, '', '15:30-17:30', '1703070653079', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 10:53:07', '2017-03-07 10:53:07'),
(103, 23, 0, '', '15:30-17:30', '1703070707369', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-07 11:07:36', '2017-03-07 11:07:36'),
(104, 1, 0, '', '10:30-11:30', '1703080911279', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', -1, '2017-03-08 01:11:27', '2017-03-08 01:11:27'),
(105, 1, 0, '', '10:30-11:30', '1703080929042', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:29:04', '2017-03-08 01:29:04'),
(106, 1, 0, '', '10:30-11:30', '1703080935125', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:35:12', '2017-03-08 01:35:12'),
(107, 1, 0, '', '10:30-11:30', '1703080937012', 0.01, 0.01, 1, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:37:01', '2017-03-08 01:37:01'),
(108, 1, 0, '', '10:30-11:30', '1703080937165', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:37:16', '2017-03-08 01:37:16'),
(109, 1, 0, '', '10:30-11:30', '1703080939325', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:39:32', '2017-03-08 01:39:32'),
(110, 21, 0, '', '10:30-11:30', '1703080942267', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:42:25', '2017-03-08 01:42:25'),
(111, 1, 0, '', '10:30-11:30', '1703080943301', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 01:43:30', '2017-03-08 01:43:30'),
(112, 1, 0, '', '10:30-11:30', '1703081021146', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 02:21:14', '2017-03-08 02:21:14'),
(113, 1, 0, '', '10:30-11:30', '1703081025327', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 02:25:32', '2017-03-08 02:25:32'),
(114, 1, 0, '', '10:30-11:30', '1703081026255', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 02:26:25', '2017-03-08 02:26:25'),
(115, 21, 0, '', '10:30-11:30', '1703081046336', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 02:46:33', '2017-03-08 02:46:33'),
(116, 21, 0, '', '10:30-11:30', '1703081048474', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 02:48:47', '2017-03-08 02:48:47'),
(117, 21, 0, '', '10:30-11:30', '1703081052202', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 02:52:20', '2017-03-08 02:52:20'),
(118, 21, 0, '', '10:30-11:30', '1703081106006', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 03:06:00', '2017-03-08 03:06:00'),
(119, 1, 0, '', '10:30-11:30', '1703081117273', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 03:17:27', '2017-03-08 03:17:27'),
(120, 1, 0, '', '10:30-11:30', '1703081120123', 0.01, 0.01, 2, 0, '', 0, 0, 12, 0, 0, '', '', 0, '2017-03-08 03:20:12', '2017-03-08 03:20:12');

-- --------------------------------------------------------

--
-- 表的结构 `order_contact`
--

CREATE TABLE `order_contact` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `phone` text,
  `province` text,
  `city` text,
  `district` text,
  `address` text,
  `postcode` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `order_contact`
--

INSERT INTO `order_contact` (`id`, `user_id`, `order_id`, `name`, `phone`, `province`, `city`, `district`, `address`, `postcode`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, 28, '老王', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 01:48:43', '2017-03-04 01:48:43'),
(2, 1, 30, 'ouba刚', '13273852162', '北京', '北京', NULL, '许昌学院静十207', NULL, NULL, '2017-03-04 06:35:33', '2017-03-04 06:35:33'),
(3, 1, 31, 'ouba刚', '13273852162', '北京', '北京', NULL, '许昌学院静十207111', NULL, NULL, '2017-03-04 07:25:46', '2017-03-04 07:25:46'),
(4, 1, 32, 'ouba刚', '13273852162', '北京', '北京', NULL, '许昌学院静十207111', NULL, NULL, '2017-03-04 07:27:04', '2017-03-04 07:27:04'),
(5, 26, 33, 'ouba刚', '13273852162', '北京', '北京', NULL, '许昌学院静十207111', NULL, NULL, '2017-03-04 07:31:24', '2017-03-04 07:31:24'),
(6, 26, 34, 'ouba刚', '13273852162', '河南', '洛阳', NULL, '许昌学院静十207111', NULL, NULL, '2017-03-04 07:33:00', '2017-03-04 07:33:00'),
(7, 24, 35, '', '', '北京', '北京', NULL, '', NULL, NULL, '2017-03-04 07:42:27', '2017-03-04 07:42:27'),
(8, 20, 36, '', '', '河南', '郑州', NULL, '', NULL, NULL, '2017-03-04 09:40:56', '2017-03-04 09:40:56'),
(9, 1, 37, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:00:40', '2017-03-04 10:00:40'),
(10, 1, 38, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:03:33', '2017-03-04 10:03:33'),
(11, 1, 39, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:07:46', '2017-03-04 10:07:46'),
(12, 1, 40, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:08:33', '2017-03-04 10:08:33'),
(13, 1, 41, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:10:13', '2017-03-04 10:10:13'),
(14, 1, 42, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:12:19', '2017-03-04 10:12:19'),
(15, 1, 43, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-04 10:47:58', '2017-03-04 10:47:58'),
(16, 23, 44, 'ouba刚', '13273852162', NULL, NULL, NULL, '许昌学院静十207', NULL, NULL, '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(17, 23, 45, '呵呵哒', '1234', NULL, NULL, NULL, '郑州金水区', NULL, NULL, '2017-03-06 06:42:35', '2017-03-06 06:42:35'),
(18, 23, 46, '呵呵哒', '1234', NULL, NULL, NULL, '郑州金水区', NULL, NULL, '2017-03-06 06:48:14', '2017-03-06 06:48:14'),
(19, 23, 47, '老王', '18303175634', NULL, NULL, NULL, '你隔壁啊', NULL, NULL, '2017-03-06 06:51:12', '2017-03-06 06:51:12'),
(20, 23, 48, 'ouba刚', '13273852162', NULL, NULL, NULL, '许昌学院静十207', NULL, NULL, '2017-03-06 06:54:52', '2017-03-06 06:54:52'),
(21, 23, 49, 'ouba刚', '13273852162', NULL, NULL, NULL, '许昌学院静十207', NULL, NULL, '2017-03-06 07:00:53', '2017-03-06 07:00:53'),
(22, 23, 50, '呵呵哒', '1234', NULL, NULL, NULL, '郑州金水区', NULL, NULL, '2017-03-06 07:22:03', '2017-03-06 07:22:03'),
(23, 23, 51, 'ouba刚', '13273852162', NULL, NULL, NULL, '许昌学院静十207', NULL, NULL, '2017-03-06 07:34:34', '2017-03-06 07:34:34'),
(24, 23, 52, 'ouba刚', '13273852162', NULL, NULL, NULL, '许昌学院静十207', NULL, NULL, '2017-03-06 10:22:10', '2017-03-06 10:22:10'),
(25, 24, 53, '', '', '北京', '北京', NULL, '', NULL, NULL, '2017-03-07 01:50:13', '2017-03-07 01:50:13'),
(26, 1, 54, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 02:05:12', '2017-03-07 02:05:12'),
(27, 1, 55, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 02:05:46', '2017-03-07 02:05:46'),
(28, 1, 56, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 02:06:31', '2017-03-07 02:06:31'),
(29, 1, 57, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 02:08:50', '2017-03-07 02:08:50'),
(30, 23, 58, 'ouba刚', '13273852162', NULL, NULL, NULL, '许昌学院静十207', NULL, NULL, '2017-03-07 02:54:04', '2017-03-07 02:54:04'),
(31, 24, 59, '', '', '北京', '北京', NULL, '', NULL, NULL, '2017-03-07 03:03:51', '2017-03-07 03:03:51'),
(32, 20, 60, '', '', '河南', '郑州', NULL, '', NULL, NULL, '2017-03-07 03:10:46', '2017-03-07 03:10:46'),
(33, 24, 62, '', '', '北京', '北京', NULL, '', NULL, NULL, '2017-03-07 03:55:30', '2017-03-07 03:55:30'),
(34, 24, 63, 'lee', '18336787690', '北京', '北京', NULL, 'aaa', NULL, NULL, '2017-03-07 04:05:01', '2017-03-07 04:05:01'),
(35, 24, 64, 'lee', '18336787690', '北京', '北京', NULL, 'aaa', NULL, NULL, '2017-03-07 06:26:45', '2017-03-07 06:26:45'),
(36, 1, 65, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 06:36:29', '2017-03-07 06:36:29'),
(37, 24, 66, 'lee', '18336787690', '北京', '北京', NULL, 'aaa', NULL, NULL, '2017-03-07 07:03:47', '2017-03-07 07:03:47'),
(38, 24, 67, 'lee', '18336787690', '北京', '北京', NULL, 'aaa', NULL, NULL, '2017-03-07 07:15:58', '2017-03-07 07:15:58'),
(39, 24, 68, 'lee', '18336787690', '北京', '北京', NULL, 'aaa', NULL, NULL, '2017-03-07 07:22:45', '2017-03-07 07:22:45'),
(40, 1, 69, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:25:55', '2017-03-07 07:25:55'),
(41, 20, 70, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 07:26:32', '2017-03-07 07:26:32'),
(42, 1, 71, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:27:22', '2017-03-07 07:27:22'),
(43, 1, 72, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:28:15', '2017-03-07 07:28:15'),
(44, 1, 73, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:29:24', '2017-03-07 07:29:24'),
(45, 20, 74, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 07:29:36', '2017-03-07 07:29:36'),
(46, 1, 75, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:30:06', '2017-03-07 07:30:06'),
(47, 1, 76, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:31:26', '2017-03-07 07:31:26'),
(48, 20, 77, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 07:31:29', '2017-03-07 07:31:29'),
(49, 1, 78, '老李', '13781120886', '北京', '北京', '东城区', '就你隔壁', NULL, NULL, '2017-03-07 07:32:01', '2017-03-07 07:32:01'),
(50, 20, 79, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 07:34:40', '2017-03-07 07:34:40'),
(51, 20, 80, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 07:37:23', '2017-03-07 07:37:23'),
(52, 20, 81, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 07:48:01', '2017-03-07 07:48:01'),
(53, 21, 82, '雪贝特', '18538553590', '北京', '北京', '东城区', '天安门', NULL, NULL, '2017-03-07 07:50:51', '2017-03-07 07:50:51'),
(54, 21, 83, '雪贝特', '18538553590', '北京', '北京', '东城区', '天安门', NULL, NULL, '2017-03-07 07:53:43', '2017-03-07 07:53:43'),
(55, 21, 84, '雪贝特', '18538553590', '北京', '北京', '东城区', '天安门', NULL, NULL, '2017-03-07 07:59:56', '2017-03-07 07:59:56'),
(56, 21, 85, '雪贝特', '18538553590', '北京', '北京', '东城区', '天安门', NULL, NULL, '2017-03-07 08:01:24', '2017-03-07 08:01:24'),
(57, 20, 86, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 08:09:03', '2017-03-07 08:09:03'),
(58, 20, 87, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 08:17:54', '2017-03-07 08:17:54'),
(59, 20, 88, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 08:21:34', '2017-03-07 08:21:34'),
(60, 20, 89, 'lee', '18336787690', '河南', '郑州', NULL, 'aaaa', NULL, NULL, '2017-03-07 08:27:24', '2017-03-07 08:27:24'),
(61, 21, 90, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-07 09:12:08', '2017-03-07 09:12:08'),
(62, 21, 91, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-07 09:24:24', '2017-03-07 09:24:24'),
(63, 21, 92, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-07 09:57:31', '2017-03-07 09:57:31'),
(64, 21, 93, '雪贝特', '18538553590', '河南', '开封', NULL, '开封市', NULL, NULL, '2017-03-07 10:05:10', '2017-03-07 10:05:10'),
(65, 23, 96, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:44:47', '2017-03-07 10:44:47'),
(66, 23, 97, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:45:21', '2017-03-07 10:45:21'),
(67, 23, 98, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:46:08', '2017-03-07 10:46:08'),
(68, 23, 99, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:52:25', '2017-03-07 10:52:25'),
(69, 23, 100, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:52:42', '2017-03-07 10:52:42'),
(70, 23, 101, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:52:46', '2017-03-07 10:52:46'),
(71, 23, 102, 'ouba刚', '18303741595', NULL, NULL, NULL, '许昌学院', NULL, NULL, '2017-03-07 10:53:07', '2017-03-07 10:53:07'),
(72, 23, 103, '呵呵哒', '1234', NULL, NULL, NULL, '郑州金水区', NULL, NULL, '2017-03-07 11:07:36', '2017-03-07 11:07:36'),
(73, 1, 104, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:11:27', '2017-03-08 01:11:27'),
(74, 1, 105, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:29:04', '2017-03-08 01:29:04'),
(75, 1, 106, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:35:12', '2017-03-08 01:35:12'),
(76, 1, 107, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:37:01', '2017-03-08 01:37:01'),
(77, 1, 108, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:37:16', '2017-03-08 01:37:16'),
(78, 1, 109, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:39:32', '2017-03-08 01:39:32'),
(79, 21, 110, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-08 01:42:25', '2017-03-08 01:42:25'),
(80, 1, 111, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 01:43:30', '2017-03-08 01:43:30'),
(81, 1, 112, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 02:21:14', '2017-03-08 02:21:14'),
(82, 1, 113, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 02:25:32', '2017-03-08 02:25:32'),
(83, 1, 114, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 02:26:25', '2017-03-08 02:26:25'),
(84, 21, 115, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-08 02:46:33', '2017-03-08 02:46:33'),
(85, 21, 116, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-08 02:48:47', '2017-03-08 02:48:47'),
(86, 21, 117, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-08 02:52:20', '2017-03-08 02:52:20'),
(87, 21, 118, '雪贝特', '18538553590', '河南', NULL, NULL, '天安门', NULL, NULL, '2017-03-08 03:06:00', '2017-03-08 03:06:00'),
(88, 1, 119, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 03:17:27', '2017-03-08 03:17:27'),
(89, 1, 120, '老李', '13781120886', '河南', NULL, NULL, '就你隔壁', NULL, NULL, '2017-03-08 03:20:12', '2017-03-08 03:20:12');

-- --------------------------------------------------------

--
-- 表的结构 `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `sku_id` text,
  `sku_name` text,
  `num` int(11) NOT NULL DEFAULT '0',
  `price` double(10,2) NOT NULL DEFAULT '0.00',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `user_id`, `name`, `sku_id`, `sku_name`, `num`, `price`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 09:44:40', '2017-03-03 09:44:40'),
(2, 2, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-03 09:47:30', '2017-03-03 09:47:30'),
(3, 3, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:04:47', '2017-03-03 10:04:47'),
(4, 4, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:18:55', '2017-03-03 10:18:55'),
(5, 5, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:22:39', '2017-03-03 10:22:39'),
(6, 6, 9, 1, '经典日韩行李箱', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:27:45', '2017-03-03 10:27:45'),
(7, 7, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-03 10:31:07', '2017-03-03 10:31:07'),
(8, 9, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-03 10:38:33', '2017-03-03 10:38:33'),
(9, 10, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:40:17', '2017-03-03 10:40:17'),
(10, 11, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-03 10:41:20', '2017-03-03 10:41:20'),
(11, 12, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-03 10:41:57', '2017-03-03 10:41:57'),
(12, 13, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:43:25', '2017-03-03 10:43:25'),
(13, 14, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:45:47', '2017-03-03 10:45:47'),
(14, 15, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:48:10', '2017-03-03 10:48:10'),
(15, 16, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:52:36', '2017-03-03 10:52:36'),
(16, 17, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:54:26', '2017-03-03 10:54:26'),
(17, 18, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 10:57:10', '2017-03-03 10:57:10'),
(18, 19, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 11:01:40', '2017-03-03 11:01:40'),
(19, 20, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 11:02:58', '2017-03-03 11:02:58'),
(20, 21, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-03 11:06:26', '2017-03-03 11:06:26'),
(21, 22, 24, 1, '111', NULL, NULL, 8, 100.00, NULL, '2017-03-03 11:10:01', '2017-03-03 11:10:01'),
(22, 23, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-03 11:18:41', '2017-03-03 11:18:41'),
(23, 24, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-04 01:31:56', '2017-03-04 01:31:56'),
(24, 25, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-04 01:34:38', '2017-03-04 01:34:38'),
(25, 26, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-04 01:38:52', '2017-03-04 01:38:52'),
(26, 27, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-04 01:39:39', '2017-03-04 01:39:39'),
(27, 28, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-04 01:48:43', '2017-03-04 01:48:43'),
(28, 29, 4, 1, '香梨', NULL, NULL, 0, 12.00, NULL, '2017-03-04 06:02:50', '2017-03-04 06:02:50'),
(29, 29, 24, 1, '111', NULL, NULL, 1, 100.00, NULL, '2017-03-04 06:02:50', '2017-03-04 06:02:50'),
(30, 30, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-04 06:35:33', '2017-03-04 06:35:33'),
(31, 30, 24, 1, '111', NULL, NULL, 3, 100.00, NULL, '2017-03-04 06:35:33', '2017-03-04 06:35:33'),
(32, 31, 4, 1, '香梨', NULL, NULL, 3, 12.00, NULL, '2017-03-04 07:25:46', '2017-03-04 07:25:46'),
(33, 32, 4, 1, '香梨', NULL, NULL, 3, 12.00, NULL, '2017-03-04 07:27:04', '2017-03-04 07:27:04'),
(34, 33, 2, 26, '香蕉', NULL, NULL, 1, 12.00, NULL, '2017-03-04 07:31:24', '2017-03-04 07:31:24'),
(35, 33, 4, 26, '香梨', NULL, NULL, 2, 12.00, NULL, '2017-03-04 07:31:24', '2017-03-04 07:31:24'),
(36, 34, 4, 26, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-04 07:33:00', '2017-03-04 07:33:00'),
(37, 35, 4, 24, '香梨', NULL, NULL, 2, 12.00, NULL, '2017-03-04 07:42:27', '2017-03-04 07:42:27'),
(38, 36, 9, 20, '经典日韩行李箱', NULL, NULL, 1, 12.00, NULL, '2017-03-04 09:40:56', '2017-03-04 09:40:56'),
(39, 37, 4, 1, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:00:40', '2017-03-04 10:00:40'),
(40, 38, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:03:33', '2017-03-04 10:03:33'),
(41, 39, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:07:46', '2017-03-04 10:07:46'),
(42, 40, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:08:33', '2017-03-04 10:08:33'),
(43, 41, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:10:13', '2017-03-04 10:10:13'),
(44, 42, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:12:19', '2017-03-04 10:12:19'),
(45, 43, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-04 10:47:58', '2017-03-04 10:47:58'),
(46, 44, 4, 23, '香梨', NULL, NULL, 1, 12.00, NULL, '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(47, 44, 10, 23, '双肩包', NULL, NULL, 2, 12.00, NULL, '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(48, 44, 23, 23, '单肩包', NULL, NULL, 1, 100.00, NULL, '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(49, 45, 16, 23, '高档提包', NULL, NULL, 3, 12.00, NULL, '2017-03-06 06:42:35', '2017-03-06 06:42:35'),
(50, 45, 1, 23, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-06 06:42:35', '2017-03-06 06:42:35'),
(51, 46, 2, 23, '香蕉', NULL, NULL, 2, 12.00, NULL, '2017-03-06 06:48:14', '2017-03-06 06:48:14'),
(52, 46, 3, 23, '苹果', NULL, NULL, 1, 12.00, NULL, '2017-03-06 06:48:14', '2017-03-06 06:48:14'),
(53, 46, 5, 23, '葡萄', NULL, NULL, 1, 12.00, NULL, '2017-03-06 06:48:14', '2017-03-06 06:48:14'),
(54, 46, 9, 23, '经典日韩行李箱', NULL, NULL, 2, 12.00, NULL, '2017-03-06 06:48:14', '2017-03-06 06:48:14'),
(55, 47, 11, 23, '军绿行礼箱', NULL, NULL, 3, 12.00, NULL, '2017-03-06 06:51:12', '2017-03-06 06:51:12'),
(56, 47, 12, 23, '粉红手提包', NULL, NULL, 1, 12.00, NULL, '2017-03-06 06:51:12', '2017-03-06 06:51:12'),
(57, 48, 13, 23, '高档手提包', NULL, NULL, 2, 12.00, NULL, '2017-03-06 06:54:52', '2017-03-06 06:54:52'),
(58, 48, 14, 23, '金属白行李箱', NULL, NULL, 1, 12.00, NULL, '2017-03-06 06:54:52', '2017-03-06 06:54:52'),
(59, 49, 25, 23, '新款牛皮斜跨小清新短款钱包', NULL, NULL, 1, 129.00, NULL, '2017-03-06 07:00:53', '2017-03-06 07:00:53'),
(60, 50, 18, 23, '小巧手提包', NULL, NULL, 1, 12.00, NULL, '2017-03-06 07:22:03', '2017-03-06 07:22:03'),
(61, 50, 21, 23, '肉末茄子', NULL, NULL, 2, 12.00, NULL, '2017-03-06 07:22:03', '2017-03-06 07:22:03'),
(62, 51, 15, 23, '红色行李箱', NULL, NULL, 3, 12.00, NULL, '2017-03-06 07:34:34', '2017-03-06 07:34:34'),
(63, 52, 4, 23, '香梨', NULL, NULL, 3, 12.00, NULL, '2017-03-06 10:22:10', '2017-03-06 10:22:10'),
(64, 53, 2, 24, '香蕉', NULL, NULL, 1, 12.00, NULL, '2017-03-07 01:50:13', '2017-03-07 01:50:13'),
(65, 54, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 02:05:12', '2017-03-07 02:05:12'),
(66, 55, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-07 02:05:46', '2017-03-07 02:05:46'),
(67, 56, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-07 02:06:31', '2017-03-07 02:06:31'),
(68, 57, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-07 02:08:50', '2017-03-07 02:08:50'),
(69, 58, 4, 23, '香梨', NULL, NULL, 7, 0.01, NULL, '2017-03-07 02:54:04', '2017-03-07 02:54:04'),
(70, 59, 3, 24, '苹果', NULL, NULL, 1, 12.00, NULL, '2017-03-07 03:03:51', '2017-03-07 03:03:51'),
(71, 60, 4, 20, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 03:10:46', '2017-03-07 03:10:46'),
(72, 62, 9, 24, '经典日韩行李箱', NULL, NULL, 1, 12.00, NULL, '2017-03-07 03:55:30', '2017-03-07 03:55:30'),
(73, 63, 5, 24, '葡萄', NULL, NULL, 1, 12.00, NULL, '2017-03-07 04:05:01', '2017-03-07 04:05:01'),
(74, 64, 10, 24, '双肩包', NULL, NULL, 1, 12.00, NULL, '2017-03-07 06:26:45', '2017-03-07 06:26:45'),
(75, 65, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 06:36:29', '2017-03-07 06:36:29'),
(76, 66, 2, 24, '香蕉', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:03:47', '2017-03-07 07:03:47'),
(77, 67, 3, 24, '苹果', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:15:58', '2017-03-07 07:15:58'),
(78, 68, 2, 24, '香蕉', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:22:45', '2017-03-07 07:22:45'),
(79, 69, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:25:55', '2017-03-07 07:25:55'),
(80, 70, 1, 20, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:26:32', '2017-03-07 07:26:32'),
(81, 71, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:27:22', '2017-03-07 07:27:22'),
(82, 72, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:28:15', '2017-03-07 07:28:15'),
(83, 73, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:29:24', '2017-03-07 07:29:24'),
(84, 74, 3, 20, '苹果', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:29:36', '2017-03-07 07:29:36'),
(85, 75, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:30:06', '2017-03-07 07:30:06'),
(86, 76, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:31:26', '2017-03-07 07:31:26'),
(87, 77, 5, 20, '葡萄', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:31:29', '2017-03-07 07:31:29'),
(88, 78, 1, 1, '橙子', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:32:01', '2017-03-07 07:32:01'),
(89, 79, 5, 20, '葡萄', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:34:40', '2017-03-07 07:34:40'),
(90, 80, 2, 20, '香蕉', NULL, NULL, 1, 12.00, NULL, '2017-03-07 07:37:23', '2017-03-07 07:37:23'),
(91, 81, 4, 20, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:48:01', '2017-03-07 07:48:01'),
(92, 82, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:50:51', '2017-03-07 07:50:51'),
(93, 83, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:53:43', '2017-03-07 07:53:43'),
(94, 84, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 07:59:56', '2017-03-07 07:59:56'),
(95, 85, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 08:01:24', '2017-03-07 08:01:24'),
(96, 86, 4, 20, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 08:09:03', '2017-03-07 08:09:03'),
(97, 87, 4, 20, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 08:17:54', '2017-03-07 08:17:54'),
(98, 88, 4, 20, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 08:21:34', '2017-03-07 08:21:34'),
(99, 89, 4, 20, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 08:27:24', '2017-03-07 08:27:24'),
(100, 90, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 09:12:08', '2017-03-07 09:12:08'),
(101, 91, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 09:24:24', '2017-03-07 09:24:24'),
(102, 92, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 09:57:31', '2017-03-07 09:57:31'),
(103, 93, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 10:05:10', '2017-03-07 10:05:10'),
(104, 94, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 10:23:40', '2017-03-07 10:23:40'),
(105, 95, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 10:26:01', '2017-03-07 10:26:01'),
(106, 96, 4, 23, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 10:44:47', '2017-03-07 10:44:47'),
(107, 97, 4, 23, '香梨', NULL, NULL, 0, 0.01, NULL, '2017-03-07 10:45:21', '2017-03-07 10:45:21'),
(108, 98, 4, 23, '香梨', NULL, NULL, 0, 0.01, NULL, '2017-03-07 10:46:08', '2017-03-07 10:46:08'),
(109, 99, 4, 23, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 10:52:25', '2017-03-07 10:52:25'),
(110, 100, 4, 23, '香梨', NULL, NULL, 0, 0.01, NULL, '2017-03-07 10:52:42', '2017-03-07 10:52:42'),
(111, 101, 4, 23, '香梨', NULL, NULL, 0, 0.01, NULL, '2017-03-07 10:52:46', '2017-03-07 10:52:46'),
(112, 102, 4, 23, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 10:53:07', '2017-03-07 10:53:07'),
(113, 103, 4, 23, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-07 11:07:36', '2017-03-07 11:07:36'),
(114, 104, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:11:27', '2017-03-08 01:11:27'),
(115, 105, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:29:04', '2017-03-08 01:29:04'),
(116, 106, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:35:12', '2017-03-08 01:35:12'),
(117, 107, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:37:01', '2017-03-08 01:37:01'),
(118, 108, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:37:16', '2017-03-08 01:37:16'),
(119, 109, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:39:32', '2017-03-08 01:39:32'),
(120, 110, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:42:25', '2017-03-08 01:42:25'),
(121, 111, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 01:43:30', '2017-03-08 01:43:30'),
(122, 112, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 02:21:14', '2017-03-08 02:21:14'),
(123, 113, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 02:25:32', '2017-03-08 02:25:32'),
(124, 114, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 02:26:25', '2017-03-08 02:26:25'),
(125, 115, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 02:46:33', '2017-03-08 02:46:33'),
(126, 116, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 02:48:47', '2017-03-08 02:48:47'),
(127, 117, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 02:52:20', '2017-03-08 02:52:20'),
(128, 118, 4, 21, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 03:06:00', '2017-03-08 03:06:00'),
(129, 119, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 03:17:27', '2017-03-08 03:17:27'),
(130, 120, 4, 1, '香梨', NULL, NULL, 1, 0.01, NULL, '2017-03-08 03:20:12', '2017-03-08 03:20:12');

-- --------------------------------------------------------

--
-- 表的结构 `order_fee`
--

CREATE TABLE `order_fee` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `value` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `order_feedback_type`
--

CREATE TABLE `order_feedback_type` (
  `id` int(11) unsigned NOT NULL,
  `name` text COMMENT '类型',
  `sub` text COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `order_feedback_type`
--

INSERT INTO `order_feedback_type` (`id`, `name`, `sub`, `status`, `created_at`, `updated_at`) VALUES
(1, '不想要了1', '这里是备注', 1, '2016-12-10 03:20:38', '2017-02-15 07:39:20'),
(2, '破了', '', 1, '2016-12-13 02:29:07', '2017-01-15 22:17:33'),
(3, '111', '222', 1, '2016-12-15 15:39:03', '2017-01-15 22:17:35'),
(4, '实物差距太大', '实物差距太大', 1, '2017-01-05 22:34:51', '2017-01-15 22:17:37'),
(5, '22', '222', 1, '2017-03-02 07:06:42', '2017-03-02 07:06:50');

-- --------------------------------------------------------

--
-- 表的结构 `payment`
--

CREATE TABLE `payment` (
  `id` tinyint(3) unsigned NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `sub` text,
  `rank` tinyint(3) NOT NULL DEFAULT '0',
  `config` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_cod` tinyint(1) NOT NULL DEFAULT '0' COMMENT '货到付款',
  `is_online` tinyint(1) NOT NULL DEFAULT '0' COMMENT '在线支付',
  `is_config` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `payment`
--

INSERT INTO `payment` (`id`, `type`, `name`, `sub`, `rank`, `config`, `status`, `is_cod`, `is_online`, `is_config`, `created_at`, `updated_at`) VALUES
(1, 'cod', '货到付款', '开通城市请详见网站公告。', 0, '', 1, 1, 0, 0, '2017-02-14 05:11:48', '2017-02-13 21:04:55'),
(2, 'wxpay', '微信支付', '微信支付，是基于微信公众号JS_API网页支付提供的支付服务功能。', 0, '{"mchid":"1243051102","key":"0IVTlxNMurPnBga1ulahd7g4m36jirgR","x_mchid":"1445117902","x_key":"8e157d6843fb72dcb27f9762308b8333"}', 1, 0, 1, 1, '2017-02-14 05:11:48', '2017-03-07 02:36:01'),
(3, 'alipay', '支付宝', '支付宝(www.alipay.com)是国内先进的网上支付平台。', 0, '{"account":"koahub@163.com","partner":"2088421387324241","key":"95ljtrm73l34qg1x5dlfyqpayx2c1c0v"}', 1, 0, 1, 1, '2017-02-14 05:11:48', '2017-02-14 05:47:00'),
(4, 'balance', '余额支付', '使用帐户余额支付。只有会员才能使用，通过设置信用额度，可以透支。', 0, '', 1, 0, 1, 0, '2017-02-14 05:11:48', '2017-02-13 02:10:32');

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `subname` text,
  `price` float NOT NULL DEFAULT '0' COMMENT '售价',
  `old_price` float NOT NULL DEFAULT '0' COMMENT '原价',
  `spec` text COMMENT '商品规格',
  `address` text COMMENT '商品产地',
  `store` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `sales` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `unit` text COMMENT '商品单位',
  `score` float NOT NULL DEFAULT '0' COMMENT '赠送积分',
  `visiter` int(11) NOT NULL DEFAULT '0' COMMENT '访问量',
  `sku` text,
  `sku_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:启用0:禁用',
  `file_id` int(11) NOT NULL DEFAULT '0' COMMENT '图片',
  `files` text COMMENT '图集',
  `detail` text,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0:隐藏，1:显示',
  `labels` text COMMENT '标签值',
  `remark` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product`
--

INSERT INTO `product` (`id`, `category_id`, `name`, `subname`, `price`, `old_price`, `spec`, `address`, `store`, `sales`, `unit`, `score`, `visiter`, `sku`, `sku_status`, `file_id`, `files`, `detail`, `status`, `labels`, `remark`, `rank`, `created_at`, `updated_at`) VALUES
(1, 1, '橙子', '美味可口', 12, 0, '', '', 5, 0, '', 12, 19, '1,2,3', 1, 12, '11,12,', '<p>1111</p>', 1, '', '', 0, '2016-07-19 01:45:09', '2017-02-24 04:00:29'),
(2, 1, '香蕉', '', 12, 0, '', '', 10, 0, '', 12, 7, '1,2,5,6,7', 1, 118, '12,', '', 1, '', '', 0, '2016-07-18 10:47:48', '2017-03-04 09:36:15'),
(3, 1, '苹果', '美味', 12, 0, '', '', 100, 0, '', 12, 13, '0', 0, 11, '', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-04 09:33:51'),
(4, 1, '香梨', '', 0.01, 1, '', '', 100, 0, '', 12, 1, '0', 0, 17, '17,117,', '<p><img src="https://img.alicdn.com/imgextra/i2/2597188887/TB2pjN0c9iJ.eBjSspoXXcpMFXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i1/2597188887/TB2yeh5c9iJ.eBjSspiXXbqAFXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i4/2597188887/TB2M.cgcygSXeFjy0FcXXahAXXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i2/2597188887/TB25L4.c3SI.eBjy1XcXXc1jXXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i1/2597188887/TB2Rnp8c39J.eBjSsppXXXAAVXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i2/2597188887/TB2et7JdCmK.eBjSZPfXXce2pXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i1/2597188887/TB2npN7c9GI.eBjSspcXXcVjFXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i1/2597188887/TB2mCQAdp5N.eBjSZFmXXboSXXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i1/2597188887/TB29WIJdzm2.eBjSZFtXXX56VXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i2/2597188887/TB2F1r_bKNOdeFjSZFBXXctzXXa_!!2597188887.jpg" class="img-ks-lazyload"/><img src="https://img.alicdn.com/imgextra/i4/2597188887/TB2649UeFXXXXbuXXXXXXXXXXXX_!!2597188887.jpg" class="img-ks-lazyload"/></p>', 1, '', '', 1111, '2016-07-20 09:13:56', '2017-03-07 01:49:43'),
(5, 1, '葡萄', NULL, 12, 0, '', '', 100, 0, '', 12, 0, '0', 0, 16, '', '', 1, '', '', 0, '2016-01-05 03:02:23', '0000-00-00 00:00:00'),
(6, 1, '菠萝', '', 12, 0, '', '', 100, 0, '', 12, 0, '1,2,3,5,6,7,9,10,11', 0, 15, '', '', 0, '', '', 0, '2016-01-05 03:02:23', '2017-02-22 08:57:47'),
(7, 1, '火龙果', '', 12, 0, '', '', 100, 0, '', 12, 2, '9,10,11', 0, 14, '', '', 0, '', '', 0, '2016-01-05 03:02:23', '2017-02-22 08:56:54'),
(8, 1, '奇异果', '', 12, 0, '', '', 100, 0, '', 12, 0, '5,6,7,8', 0, 13, '', '', 0, '', '', 0, '2016-01-05 03:02:23', '2017-02-22 08:56:44'),
(9, 1, '经典日韩行李箱', '', 12, 0, '', '', 100, 0, '', 12, 1, '1,2,3,4', 0, 90, '90,', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:31:24'),
(10, 2, '双肩包', '', 12, 0, '', '', 100, 0, '', 12, 0, '1,2,3,5,6', 0, 91, '91,', '', 1, '', '', 100, '2016-01-05 03:02:23', '2017-03-03 02:00:27'),
(11, 2, '军绿行礼箱', '', 12, 0, '', '', 100, 0, '', 12, 0, '1,2,5,6,7', 0, 92, '92,', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:29:28'),
(12, 2, '粉红手提包', '', 12, 0, '', '', 100, 0, '', 12, 1, '1,2,4', 0, 93, '93,', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:27:25'),
(13, 2, '高档手提包', '', 12, 0, '', '', 100, 0, '', 12, 0, '9,10,11', 0, 81, '81,', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:23:52'),
(14, 2, '金属白行李箱', '', 12, 0, '', '', 100, 0, '', 12, 0, '5,6', 0, 80, '80,', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:22:58'),
(15, 3, '红色行李箱', '', 12, 0, '', '', 100, 0, '', 12, 0, '1,2', 0, 86, '86,', '', 1, '', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:22:04'),
(16, 2, '高档提包', '', 12, 0, '', '', 100, 0, '', 12, 0, '9,10,11', 0, 84, '84,', '', 1, '', '', 20, '2016-01-05 03:02:23', '2017-03-03 01:20:56'),
(17, 2, '经典行李箱', '', 12, 0, '', '', 100, 0, '', 12, 0, '5,6', 0, 82, '82,', '', 1, '2', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:19:13'),
(18, 2, '小巧手提包', '', 12, 0, '', '', 100, 0, '', 12, 0, '1,2,3', 0, 87, '87,', '', 1, '1', '', 0, '2016-01-05 03:02:23', '2017-03-01 09:20:23'),
(19, 1, '红烧排骨11', '11', 1211, 1, '', '', 0, 0, '11', 12, 1, '1,2,5,6,7,8', 1, 83, '83,', '', 1, '1', '1', 0, '2016-01-11 10:39:35', '2017-03-02 08:43:24'),
(21, 3, '肉末茄子', '好吃不贵', 12, 0.2, '', '', 100, 1, '千克', 12, 3, '1,2,3,5,6,7,8', 1, 88, '84,88,', '<p>箱包详情<img src="http://img.baidu.com/hi/jx2/j_0028.gif"/><img src="http://192.168.0.106/public/ueditor/php/upload/image/20170222/1487761361901759.jpg" title="1487761361901759.jpg" alt="403525e0-b3bb-11e6-b639-bf5cd88ca9e8.jpg"/></p><p style="display: none;"><br/></p>', 1, '1,2', '', 0, '2016-03-10 07:30:14', '2017-03-01 09:33:24'),
(23, 3, '单肩包', '', 100, 120, '', '', 10, 0, '元', 10, 0, NULL, 0, 109, '109,', '<p><img src="http://192.168.0.107/public/ueditor/php/upload/image/20170302/1488449575940167.png" title="1488449575940167.png" alt="longchamp_crossbody_bags_roseau_sakura_1016873A26_0.png" width="304" height="287"/></p>', 1, '1,2,3', '', 22, '2017-03-02 09:26:46', '2017-03-02 10:16:12'),
(24, 1, '111', '11', 100, 120, '', '', 100, 0, '元', 11, 0, NULL, 0, 110, '110,', '<p><img src="http://192.168.0.107/public/ueditor/php/upload/image/20170302/1488449467856217.jpeg" title="1488449467856217.jpeg" alt="58b56445N7a3876dc.jpeg"/></p><p>&nbsp; &nbsp; &nbsp; &nbsp;进口的说<img src="http://192.168.0.107/public/ueditor/php/upload/image/20170302/1488452758963934.jpeg" title="1488452758963934.jpeg" alt="d2b2275ac43cc8bce03760c162d23724ca67b803122af-qLuXr8_fw658.jpeg" width="327" height="342"/>法和刚开始</p><p style="text-align:center"><img src="http://img.baidu.com/hi/jx2/j_0015.gif"/></p><p><br/></p>', 1, '1,2,3', '1', 0, '2017-03-02 10:05:38', '2017-03-04 09:32:26'),
(25, 4, '新款牛皮斜跨小清新短款钱包', '', 129, 168, '', '', 100, 0, '', 100, 0, NULL, 0, 112, '112,', '<p><img src="http://192.168.0.107/public/ueditor/php/upload/image/20170302/1488450522428302.jpg" title="1488450522428302.jpg" alt="58abb1eaN847f21e1.jpg"/></p>', 1, '1,2,3', '', 0, '2017-03-02 10:28:50', '2017-03-02 10:29:17'),
(26, 4, '111', '11', 11, 0, '', '', 0, 0, '111', 11, 0, NULL, 0, 112, '112,', '<p>11</p>', 1, '1', '', 0, '2017-03-02 10:30:59', '2017-03-02 10:30:59');

-- --------------------------------------------------------

--
-- 表的结构 `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `pid` int(11) NOT NULL DEFAULT '0',
  `file_id` int(11) NOT NULL DEFAULT '0',
  `remark` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product_category`
--

INSERT INTO `product_category` (`id`, `name`, `pid`, `file_id`, `remark`, `rank`, `created_at`, `updated_at`) VALUES
(1, '精品精品', 0, 83, '', 23, '2015-11-18 02:25:27', '2017-03-07 06:56:00'),
(2, '顶级', 0, 87, '', 15, '2015-11-06 08:05:39', '2017-03-02 10:12:15'),
(3, '日韩', 0, 82, '12', 13, '2015-11-06 08:06:06', '2017-03-03 01:55:27'),
(4, '流行精', 0, 90, '', 22, '2016-01-05 02:30:11', '2017-03-07 06:56:05'),
(8, '春夏', 0, 0, '', 0, '2017-03-02 10:17:43', '2017-03-02 10:17:43'),
(9, '优雅', 0, 0, '', 0, '2017-03-02 10:17:51', '2017-03-02 10:17:51'),
(10, '复古', 0, 0, '', 12, '2017-03-02 10:18:03', '2017-03-03 01:55:35');

-- --------------------------------------------------------

--
-- 表的结构 `product_comment`
--

CREATE TABLE `product_comment` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `star` int(11) NOT NULL DEFAULT '0' COMMENT '星星5',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:显示0:隐藏',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product_comment`
--

INSERT INTO `product_comment` (`id`, `product_id`, `user_id`, `name`, `star`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '不错不错', 0, 1, NULL, '2017-02-14 03:56:22', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `product_exchange`
--

CREATE TABLE `product_exchange` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL DEFAULT '0',
  `remark` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '-1:拒绝0:未处理1:通过',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product_exchange`
--

INSERT INTO `product_exchange` (`id`, `order_id`, `product_id`, `reason_id`, `remark`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 21, 1, 'aaa', -1, '2016-12-28 15:03:59', '2016-12-28 17:40:19');

-- --------------------------------------------------------

--
-- 表的结构 `product_label`
--

CREATE TABLE `product_label` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `subname` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product_label`
--

INSERT INTO `product_label` (`id`, `name`, `subname`, `remark`, `created_at`, `updated_at`) VALUES
(1, '热销', '热销', '121', '2016-01-05 08:48:11', '2017-02-10 01:46:53'),
(2, '推荐', '推荐', '', '2016-01-05 08:55:11', '0000-00-00 00:00:00'),
(3, '特卖', '特卖', '', '2016-01-07 08:00:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `product_sku`
--

CREATE TABLE `product_sku` (
  `id` int(11) unsigned NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `ids` text COMMENT 'sku-id',
  `name` text COMMENT 'sku-name',
  `price` float NOT NULL DEFAULT '0' COMMENT '售价',
  `old_price` float NOT NULL DEFAULT '0' COMMENT '原价',
  `store` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `sales` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product_sku`
--

INSERT INTO `product_sku` (`id`, `product_id`, `ids`, `name`, `price`, `old_price`, `store`, `sales`, `created_at`, `updated_at`) VALUES
(9, 19, '2-6', '红-X', 0, 0, 0, 0, '2017-02-13 04:47:22', '2017-02-22 15:26:28'),
(10, 19, '2-7', '红-XL', 0, 0, 0, 0, '2017-02-13 04:47:22', '2017-02-22 15:26:28'),
(11, 19, '2-8', '红-2XL', 0, 0, 0, 0, '2017-02-13 04:47:30', '2017-02-22 15:26:28'),
(21, 21, '3-6', '黄-X', 0, 0, 0, 0, '2017-02-15 06:57:18', '2017-02-22 15:25:54'),
(22, 21, '3-7', '黄-XL', 0, 0, 0, 0, '2017-02-15 06:57:18', '2017-02-22 15:25:54'),
(24, 21, '2-6', '红-X', 0, 0, 0, 0, '2017-02-15 06:57:49', '2017-02-22 15:25:54'),
(25, 21, '2-7', '红-XL', 0, 0, 0, 0, '2017-02-15 06:57:49', '2017-02-22 15:25:54'),
(27, 18, '2', '红', 1, 1, 2, 2, '2017-02-22 08:54:27', '2017-02-22 08:54:27'),
(28, 18, '3', '黄', 2, 2, 2, 2, '2017-02-22 08:54:27', '2017-02-22 08:54:27'),
(29, 17, '6', 'X', 1, 1, 1, 1, '2017-02-22 08:54:43', '2017-02-22 08:54:43'),
(30, 16, '10', '大', 0, 0, 0, 0, '2017-02-22 08:55:01', '2017-02-22 08:55:01'),
(31, 16, '11', '小', 0, 0, 0, 0, '2017-02-22 08:55:01', '2017-02-22 08:55:01'),
(32, 15, '2', '红', 0, 0, 0, 0, '2017-02-22 08:55:17', '2017-02-22 08:55:17'),
(33, 14, '6', 'X', 0, 0, 0, 0, '2017-02-22 08:55:26', '2017-02-22 08:55:26'),
(34, 13, '10', '大', 0, 0, 0, 0, '2017-02-22 08:55:35', '2017-02-22 08:55:35'),
(35, 13, '11', '小', 0, 0, 0, 0, '2017-02-22 08:55:35', '2017-02-22 08:55:35'),
(36, 12, '2', '红', 0, 0, 0, 0, '2017-02-22 08:55:46', '2017-02-22 08:55:46'),
(37, 12, '4', '蓝', 0, 0, 0, 0, '2017-02-22 08:55:46', '2017-02-22 08:55:46'),
(38, 11, '2-6', '红-X', 0, 0, 0, 0, '2017-02-22 08:56:03', '2017-02-22 08:56:03'),
(39, 11, '2-7', '红-XL', 0, 0, 0, 0, '2017-02-22 08:56:03', '2017-02-22 08:56:03'),
(40, 10, '2-6', '红-X', 0, 0, 0, 0, '2017-02-22 08:56:19', '2017-02-22 08:56:19'),
(41, 10, '3-6', '黄-X', 0, 0, 0, 0, '2017-02-22 08:56:19', '2017-02-22 08:56:19'),
(42, 9, '2', '红', 0, 0, 0, 0, '2017-02-22 08:56:34', '2017-02-22 08:56:34'),
(43, 9, '3', '黄', 0, 0, 0, 0, '2017-02-22 08:56:34', '2017-02-22 08:56:34'),
(44, 9, '4', '蓝', 0, 0, 0, 0, '2017-02-22 08:56:34', '2017-02-22 08:56:34'),
(45, 8, '6', 'X', 0, 0, 0, 0, '2017-02-22 08:56:44', '2017-02-22 08:56:44'),
(46, 8, '7', 'XL', 0, 0, 0, 0, '2017-02-22 08:56:44', '2017-02-22 08:56:44'),
(47, 8, '8', '2XL', 0, 0, 0, 0, '2017-02-22 08:56:44', '2017-02-22 08:56:44'),
(48, 7, '10', '大', 0, 0, 0, 0, '2017-02-22 08:56:54', '2017-02-22 08:56:54'),
(49, 7, '11', '小', 0, 0, 0, 0, '2017-02-22 08:56:54', '2017-02-22 08:56:54'),
(54, 6, '2-6-10', '红-X-大', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(55, 6, '2-6-11', '红-X-小', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(56, 6, '2-7-10', '红-XL-大', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(57, 6, '2-7-11', '红-XL-小', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(58, 6, '3-6-10', '黄-X-大', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(59, 6, '3-6-11', '黄-X-小', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(60, 6, '3-7-10', '黄-XL-大', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(61, 6, '3-7-11', '黄-XL-小', 0, 0, 0, 0, '2017-02-22 08:57:47', '2017-02-22 08:57:47'),
(62, 21, '2-8', '红-2XL', 0, 0, 0, 0, '2017-02-22 15:01:25', '2017-02-22 15:25:54'),
(63, 21, '3-8', '黄-2XL', 0, 0, 0, 0, '2017-02-22 15:01:25', '2017-02-22 15:25:54'),
(64, 1, '2', '红', 0, 0, 0, 0, '2017-02-24 04:00:29', '2017-02-24 04:00:29'),
(65, 1, '3', '黄', 0, 0, 0, 0, '2017-02-24 04:00:29', '2017-02-24 04:00:29'),
(66, 2, '2-6', '红-X', 0, 0, 0, 0, '2017-02-24 04:00:45', '2017-02-24 04:00:45'),
(67, 2, '2-7', '红-XL', 0, 0, 0, 0, '2017-02-24 04:00:45', '2017-02-24 04:00:45'),
(68, 22, '6', 'X', 0, 0, 0, 0, '2017-03-02 07:02:36', '2017-03-02 07:02:36'),
(69, 22, '7', 'XL', 0, 0, 0, 0, '2017-03-02 07:02:36', '2017-03-02 07:02:36'),
(70, 22, '8', '2XL', 0, 0, 0, 0, '2017-03-02 07:02:36', '2017-03-02 07:02:36');

-- --------------------------------------------------------

--
-- 表的结构 `sku`
--

CREATE TABLE `sku` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级id',
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sku`
--

INSERT INTO `sku` (`id`, `pid`, `text`, `created_at`, `updated_at`) VALUES
(1, 0, '颜色', '2016-11-28 02:22:44', '2017-01-18 09:51:30'),
(2, 1, '红', '2016-11-28 02:22:44', '2017-02-08 13:41:29'),
(3, 1, '黄', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(4, 1, '蓝', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(5, 0, '尺寸', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(6, 5, 'X', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(7, 5, 'XL', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(8, 5, '2XL', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(9, 0, '大小', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(10, 9, '大', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(11, 9, '小', '2017-01-18 02:37:36', '2017-01-18 02:38:00');

-- --------------------------------------------------------

--
-- 表的结构 `sms`
--

CREATE TABLE `sms` (
  `id` int(10) unsigned NOT NULL,
  `app_key` text,
  `app_secret` text,
  `sign` text COMMENT '短信签名',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sms`
--

INSERT INTO `sms` (`id`, `app_key`, `app_secret`, `sign`, `created_at`, `updated_at`) VALUES
(1, '23643042', '17f701feb8fd1a0f3c376d4eaaa2710b', 'tp商城', '2016-07-19 09:38:40', '2017-03-02 06:48:05');

-- --------------------------------------------------------

--
-- 表的结构 `sms_tpl`
--

CREATE TABLE `sms_tpl` (
  `id` int(11) unsigned NOT NULL,
  `type` text COMMENT '类型',
  `name` text COMMENT '模版名',
  `template_code` text COMMENT '模版ID',
  `content` text COMMENT '内容',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态1:开启0:关闭',
  `phone` text COMMENT '测试发送邮箱',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sms_tpl`
--

INSERT INTO `sms_tpl` (`id`, `type`, `name`, `template_code`, `content`, `status`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'register', '短信验证码', 'SMS_47900069', '您的本次验证码${code}，10分钟内输入有效，感谢使用平台', 1, '15238027761', '0000-00-00 00:00:00', '2017-02-18 09:13:21');

-- --------------------------------------------------------

--
-- 表的结构 `sms_verify`
--

CREATE TABLE `sms_verify` (
  `id` int(11) unsigned NOT NULL,
  `uuid` text,
  `phone` text,
  `code` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sms_verify`
--

INSERT INTO `sms_verify` (`id`, `uuid`, `phone`, `code`, `created_at`, `updated_at`) VALUES
(1, '10bcd430-f802-11e6-a56c-38c986416005', '18538753627', '549955', '2017-02-21 06:50:43', '2017-02-21 06:50:43'),
(2, '55de33ee-f97c-11e6-a831-38c986416005', '18538553590', '843757', '2017-02-23 03:58:29', '2017-02-23 03:58:29'),
(3, 'e6505648-f98e-11e6-a294-38c986416005', '18538553590', '617078', '2017-02-23 06:11:22', '2017-02-23 06:11:22'),
(4, '6235677e-f991-11e6-b0be-38c986416005', '18538553590', '514158', '2017-02-23 06:29:09', '2017-02-23 06:29:09'),
(5, '8f10c82e-f991-11e6-acb9-38c986416005', '18336787690', '721361', '2017-02-23 06:30:24', '2017-02-23 06:30:24'),
(6, '962e2cbe-f996-11e6-8fd8-38c986416005', '18336787690', '396566', '2017-02-23 07:06:23', '2017-02-23 07:06:23'),
(7, 'fb92aefe-f996-11e6-80e4-38c986416005', '18336787690', '785117', '2017-02-23 07:09:14', '2017-02-23 07:09:14'),
(8, 'df2793f0-f997-11e6-8dc1-38c986416005', '18336787690', '166511', '2017-02-23 07:15:35', '2017-02-23 07:15:35'),
(9, '8c4c915e-f99b-11e6-a853-38c986416005', '18336787690', '796243', '2017-02-23 07:41:54', '2017-02-23 07:41:54'),
(10, '34023c14-f99c-11e6-a019-38c986416005', '18336787690', '877669', '2017-02-23 07:46:36', '2017-02-23 07:46:36'),
(11, '397d3e3a-f99e-11e6-a183-38c986416005', '18336787690', '582168', '2017-02-23 08:01:04', '2017-02-23 08:01:04'),
(12, '91199e4a-fa34-11e6-a752-38c986416005', '15378708792', '584488', '2017-02-24 01:57:16', '2017-02-24 01:57:16'),
(13, '27622e12-fa35-11e6-b36c-38c986416005', '15378708792', '750316', '2017-02-24 02:01:28', '2017-02-24 02:01:28'),
(14, '260fa778-fa40-11e6-bb9b-38c986416005', '15378708792', '864204', '2017-02-24 03:20:09', '2017-02-24 03:20:09'),
(15, 'be484234-fa40-11e6-8cfa-38c986416005', '15378708792', '838593', '2017-02-24 03:24:25', '2017-02-24 03:24:25'),
(16, 'f7b6c72a-fa40-11e6-89e8-38c986416005', '15378708792', '913415', '2017-02-24 03:26:02', '2017-02-24 03:26:02'),
(17, 'eb942bfc-fa5b-11e6-ac6d-38c986416005', '18538553590', '642531', '2017-02-24 06:38:57', '2017-02-24 06:38:57'),
(18, 'c84ee8b4-fa7c-11e6-96de-38c986416005', '18336787690', '753195', '2017-02-24 10:34:12', '2017-02-24 10:34:12'),
(19, 'd32852ba-fa7d-11e6-a4db-38c986416005', '18336787690', '422289', '2017-02-24 10:41:40', '2017-02-24 10:41:40'),
(20, '1894a6d2-fa7e-11e6-9611-38c986416005', '18538753627', '275405', '2017-02-24 10:43:36', '2017-02-24 10:43:36'),
(21, '48fee42c-fa7e-11e6-ad37-38c986416005', '18538753627', '308212', '2017-02-24 10:44:57', '2017-02-24 10:44:57'),
(22, 'b599c0a2-fa7e-11e6-b844-38c986416005', '18538753627', '614586', '2017-02-24 10:47:59', '2017-02-24 10:47:59'),
(23, 'aaf845ac-fa82-11e6-9c53-38c986416005', '15238027761', '876302', '2017-02-24 11:16:20', '2017-02-24 11:16:20'),
(24, 'b79f31a2-fd99-11e6-ba46-38c986416005', '18538553590', '887518', '2017-02-28 09:38:53', '2017-02-28 09:38:53'),
(25, 'e456a676-fd99-11e6-9c24-38c986416005', '18538553590', '998530', '2017-02-28 09:40:08', '2017-02-28 09:40:08'),
(26, '7ff11b18-fe29-11e6-9ec8-38c986416005', '13838309026', '616679', '2017-03-01 02:48:07', '2017-03-01 02:48:07'),
(27, '7ed46c12-fe29-11e6-a1d4-38c986416005', '13838309026', '571497', '2017-03-01 02:48:05', '2017-03-01 02:48:05'),
(28, 'e8a8dfd8-fe29-11e6-9471-38c986416005', '13838309026', '527011', '2017-03-01 02:51:02', '2017-03-01 02:51:02'),
(29, '9cb1f320-fe2a-11e6-aeca-38c986416005', '13838309026', '334330', '2017-03-01 02:56:05', '2017-03-01 02:56:05'),
(30, '983f7bde-fe5f-11e6-818e-38c986416005', '13273852162', '531659', '2017-03-01 09:15:20', '2017-03-01 09:15:20'),
(31, 'da0e060c-fe5f-11e6-aee2-38c986416005', '13273852162', '414536', '2017-03-01 09:17:11', '2017-03-01 09:17:11'),
(32, '48925ebe-fe62-11e6-9bd6-38c986416005', '13273852162', '456642', '2017-03-01 09:34:35', '2017-03-01 09:34:35'),
(33, '88a7cbf6-fe62-11e6-9a1a-38c986416005', '13273852162', '958607', '2017-03-01 09:36:23', '2017-03-01 09:36:23'),
(34, 'c3b1eca4-fe62-11e6-b031-38c986416005', '13273852162', '280657', '2017-03-01 09:38:02', '2017-03-01 09:38:02'),
(35, 'ea615c68-fe62-11e6-8189-38c986416005', '13273852162', '899725', '2017-03-01 09:39:07', '2017-03-01 09:39:07'),
(36, '0bb5679c-fe63-11e6-9b3c-38c986416005', '13273852162', '366650', '2017-03-01 09:40:03', '2017-03-01 09:40:03'),
(37, 'eef04c20-fe63-11e6-a9d9-38c986416005', '13871120863', '893706', '2017-03-01 09:46:24', '2017-03-01 09:46:24'),
(38, '82526bce-fe64-11e6-a93a-38c986416005', '15738361451', '788680', '2017-03-01 09:50:31', '2017-03-01 09:50:31'),
(39, 'e2ef46c8-fe64-11e6-8784-38c986416005', '15738361451', '182477', '2017-03-01 09:53:13', '2017-03-01 09:53:13'),
(40, '1c0de396-fe66-11e6-a718-38c986416005', '15738361451', '981431', '2017-03-01 10:01:59', '2017-03-01 10:01:59'),
(41, '79aa41f2-fe66-11e6-a37d-38c986416005', '15738361451', '261153', '2017-03-01 10:04:36', '2017-03-01 10:04:36'),
(42, '8e4de50e-fe67-11e6-a4c9-38c986416005', '13838309026', '931835', '2017-03-01 10:12:20', '2017-03-01 10:12:20'),
(43, '338ecae2-fe6d-11e6-b62f-38c986416005', '15738361451', '146748', '2017-03-01 10:52:45', '2017-03-01 10:52:45'),
(44, '3c6af4fc-fee9-11e6-9aa3-38c986416005', '13273852162', '130190', '2017-03-02 01:40:37', '2017-03-02 01:40:37'),
(45, '970ebcc2-fee9-11e6-9eff-38c986416005', '18336787690', '503824', '2017-03-02 01:43:09', '2017-03-02 01:43:09'),
(46, 'd79b82d4-feee-11e6-a7a2-38c986416005', '13273852162', '256940', '2017-03-02 02:20:45', '2017-03-02 02:20:45'),
(47, 'e3d3e840-fef1-11e6-913f-38c986416005', '13273852162', '456742', '2017-03-02 02:42:34', '2017-03-02 02:42:34'),
(48, '65f2c13e-fef2-11e6-b113-38c986416005', '15738361451', '294578', '2017-03-02 02:46:12', '2017-03-02 02:46:12'),
(49, '4981c4d4-ff09-11e6-ab27-38c986416005', '13273852162', '248405', '2017-03-02 05:30:03', '2017-03-02 05:30:03'),
(50, '06cc6cfe-00ab-11e7-8e33-38c986416005', '13273852162', '151643', '2017-03-04 07:20:20', '2017-03-04 07:20:20'),
(51, '0dccc0ba-00ad-11e7-b7c8-38c986416005', '13273852162', '756617', '2017-03-04 07:34:51', '2017-03-04 07:34:51'),
(52, '06d0ce0e-00ae-11e7-9391-38c986416005', '13273852162', '539511', '2017-03-04 07:41:49', '2017-03-04 07:41:49'),
(53, '715a4eec-0259-11e7-9cf7-38c986416005', '13017553653', '407104', '2017-03-06 10:41:23', '2017-03-06 10:41:23'),
(54, 'd97ffdf4-025a-11e7-8aae-38c986416005', '13017553653', '951462', '2017-03-06 10:51:27', '2017-03-06 10:51:27'),
(55, '5561220e-02d3-11e7-8a70-38c986416005', '13017553653', '827701', '2017-03-07 01:13:55', '2017-03-07 01:13:55'),
(56, '86e6aa92-02d3-11e7-b998-38c986416005', '13017553653', '412484', '2017-03-07 01:15:18', '2017-03-07 01:15:18'),
(57, '51772074-02d5-11e7-9a29-38c986416005', '13017553653', '396202', '2017-03-07 01:28:07', '2017-03-07 01:28:07'),
(58, 'c9a83812-02d5-11e7-a46b-38c986416005', '13017553653', '474155', '2017-03-07 01:31:29', '2017-03-07 01:31:29'),
(59, 'aca1582e-02d6-11e7-97f7-38c986416005', '13017553653', '799985', '2017-03-07 01:37:49', '2017-03-07 01:37:49'),
(60, '5ce1f764-02e8-11e7-8424-38c986416005', '15738361451', '172254', '2017-03-07 03:44:27', '2017-03-07 03:44:27'),
(61, '7df4c7d2-02e9-11e7-b67f-38c986416005', '15663883302', '860364', '2017-03-07 03:52:32', '2017-03-07 03:52:32'),
(62, '956f4a2c-02e9-11e7-8fb2-38c986416005', '15603883302', '538550', '2017-03-07 03:53:11', '2017-03-07 03:53:11'),
(63, 'a62ea6d8-02fc-11e7-bb10-38c986416005', '18538553590', '597243', '2017-03-07 06:09:39', '2017-03-07 06:09:39'),
(64, 'c6c215d2-02fd-11e7-9369-38c986416005', '15738361451', '267300', '2017-03-07 06:17:44', '2017-03-07 06:17:44'),
(65, 'ac371d4a-0305-11e7-aaa7-38c986416005', '18538553590', '416715', '2017-03-07 07:14:15', '2017-03-07 07:14:15'),
(66, '011707c6-0306-11e7-9d79-38c986416005', '18538553590', '943045', '2017-03-07 07:16:37', '2017-03-07 07:16:37'),
(67, '9acbb44c-0307-11e7-9d61-38c986416005', '15738361451', '307583', '2017-03-07 07:28:05', '2017-03-07 07:28:05'),
(68, '7d6aacda-0309-11e7-ae58-38c986416005', '15738361451', '632967', '2017-03-07 07:41:34', '2017-03-07 07:41:34'),
(69, '633af5aa-039e-11e7-b65f-38c986416005', '13073720275', '950269', '2017-03-08 01:27:26', '2017-03-08 01:27:26'),
(70, '86d4ae24-03a4-11e7-91bd-38c986416005', '13073720275', '967254', '2017-03-08 02:11:22', '2017-03-08 02:11:22'),
(71, '1e5335ea-03a5-11e7-b9e3-38c986416005', '15738361451', '692040', '2017-03-08 02:15:37', '2017-03-08 02:15:37'),
(72, 'd8f6347e-03a5-11e7-90ef-38c986416005', '13073720275', '871815', '2017-03-08 02:20:50', '2017-03-08 02:20:50'),
(73, '19aacd9e-03a7-11e7-b80e-38c986416005', '13073720275', '511859', '2017-03-08 02:29:48', '2017-03-08 02:29:48'),
(74, '0fe4a652-03d6-11e7-aa50-38c986416005', '13017553653', '417865', '2017-03-08 08:05:58', '2017-03-08 08:05:58'),
(75, '523a0e02-03d6-11e7-bdcb-38c986416005', '13460369051', '498375', '2017-03-08 08:07:49', '2017-03-08 08:07:49'),
(76, 'c1f2bfb2-03d8-11e7-930c-38c986416005', '13073020275', '654996', '2017-03-08 08:25:15', '2017-03-08 08:25:15'),
(77, 'f767458c-03d8-11e7-b1c9-38c986416005', '13073720275', '603345', '2017-03-08 08:26:45', '2017-03-08 08:26:45'),
(78, '226302c6-03d9-11e7-8084-38c986416005', '13073720275', '979838', '2017-03-08 08:27:57', '2017-03-08 08:27:57'),
(79, 'c3f5b6ec-055a-11e7-b0de-38c986416005', '15670560752', '775767', '2017-03-10 06:28:24', '2017-03-10 06:28:24'),
(80, 'f3a4b49c-055a-11e7-bc78-38c986416005', '18749860134', '911558', '2017-03-10 06:29:44', '2017-03-10 06:29:44'),
(81, '591aad3a-055c-11e7-9c9f-38c986416005', '15670560752', '858041', '2017-03-10 06:39:44', '2017-03-10 06:39:44');

-- --------------------------------------------------------

--
-- 表的结构 `trade`
--

CREATE TABLE `trade` (
  `id` int(10) unsigned NOT NULL,
  `tradeid` text,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `payment` text COMMENT '支付方式',
  `money` float(10,2) NOT NULL DEFAULT '0.00',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1:充值0:消费',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:成功0:失败',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `trade`
--

INSERT INTO `trade` (`id`, `tradeid`, `user_id`, `payment`, `money`, `type`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, '1701031759007', 1, '支付宝', 10.00, 0, 0, NULL, '2017-02-15 13:40:33', '0000-00-00 00:00:00'),
(2, '54824555478454', 1, '微信支付', 2.00, 1, 1, NULL, '2017-02-15 14:21:12', '0000-00-00 00:00:00'),
(3, '1703020723355', 1, '余额支付', 12.00, 0, 0, NULL, '2017-03-02 11:23:38', '2017-03-02 11:23:38'),
(4, '1703030354577', 1, '余额支付', 100.00, 0, 0, NULL, '2017-03-03 07:54:59', '2017-03-03 07:54:59'),
(5, '1703030631505', 1, '余额支付', 0.00, 0, 0, NULL, '2017-03-03 10:31:52', '2017-03-03 10:31:52'),
(6, '21703070543247', 21, '', 1.00, 1, 0, NULL, '2017-03-07 09:43:24', '2017-03-07 09:43:24'),
(7, '21703070543591', 21, '', 1.00, 1, 0, NULL, '2017-03-07 09:43:59', '2017-03-07 09:43:59'),
(8, '21703070544488', 21, '', 1.00, 1, 0, NULL, '2017-03-07 09:44:48', '2017-03-07 09:44:48'),
(9, '21703070545114', 21, '', 1.00, 1, 0, NULL, '2017-03-07 09:45:11', '2017-03-07 09:45:11'),
(10, '21703070549301', 21, '', 1.00, 1, 0, NULL, '2017-03-07 09:49:30', '2017-03-07 09:49:30'),
(11, '21703070552519', 21, '', 1.00, 1, 0, NULL, '2017-03-07 09:52:51', '2017-03-07 09:52:51'),
(12, '21703081042012', 21, '', 1.00, 1, 0, NULL, '2017-03-08 02:42:01', '2017-03-08 02:42:01');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL,
  `contact_id` int(11) NOT NULL DEFAULT '0' COMMENT '默认地址',
  `avater_id` int(11) NOT NULL DEFAULT '0' COMMENT '头像',
  `nickname` text,
  `username` text,
  `phone` text,
  `password` text,
  `token` text,
  `money` float NOT NULL DEFAULT '0',
  `score` float NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:启用0:禁用',
  `buy_num` int(11) NOT NULL DEFAULT '0' COMMENT '用户购买量',
  `remark` text,
  `last_login_ip` text,
  `last_login_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `contact_id`, `avater_id`, `nickname`, `username`, `phone`, `password`, `token`, `money`, `score`, `status`, `buy_num`, `remark`, `last_login_ip`, `last_login_time`, `created_at`, `updated_at`) VALUES
(1, 3, 78, NULL, 'wemall', '1', 'c4ca4238a0b923820dcc509a6f75849b', '', 2, 420, 1, 192, '12', '192.168.0.120', '2017-03-08 03:17:11', '2016-07-26 02:14:20', '2017-03-08 03:17:11'),
(2, 6, 2, NULL, 'pidong', '18737131820', '', '7R6LhOAOGi3BnyBeRk316PAcR8OCOTS', 10, 10, 1, 0, '12', '', '0000-00-00 00:00:00', '2016-07-26 02:12:16', '2017-02-15 09:46:53'),
(3, 7, 3, NULL, '雪贝特', '18538553590', 'e592f69a66363980b3756741f2195f35', NULL, 0, 0, 1, 0, NULL, '192.168.0.120', '2017-03-01 01:42:20', '2017-02-23 06:37:59', '2017-03-01 01:42:20'),
(5, 0, 0, NULL, '1', '15378708792', 'c81e728d9d4c2f636f067f89cc14862c', NULL, 10000, 0, 1, 0, NULL, '127.0.0.1', '2017-02-28 01:30:12', '2017-02-24 02:22:07', '2017-02-28 01:30:12'),
(6, 0, 0, NULL, '18538753627', '18538753627', 'c33367701511b4f6020ec61ded352059', NULL, 0, 0, 1, 0, NULL, '192.168.0.100', '2017-02-24 10:48:44', '2017-02-24 10:44:11', '2017-02-24 10:48:44'),
(14, 0, 0, NULL, 'AI.何青', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-02-28 03:11:15', '2017-02-28 03:11:15'),
(20, 0, 77, NULL, 'Greg.荣', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-02-28 09:28:42', '2017-02-28 09:28:42'),
(21, 14, 78, NULL, '国恒', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-02-28 09:30:55', '2017-02-28 09:30:55'),
(22, 0, 0, NULL, '13838309026', '13838309026', 'e10adc3949ba59abbe56e057f20f883e', NULL, 0, 500, 1, 0, '', '192.168.0.117', '2017-03-02 01:23:14', '2017-03-01 02:51:24', '2017-03-02 09:43:53'),
(23, 0, 0, NULL, 'ouba', '13273852162', '202cb962ac59075b964b07152d234b70', NULL, 0, 0, 1, 0, NULL, '192.168.0.116', '2017-03-10 05:46:29', '2017-03-02 01:40:51', '2017-03-10 05:46:29'),
(24, 0, 0, NULL, '18336787690', '18336787690', 'e10adc3949ba59abbe56e057f20f883e', NULL, 0, 0, 1, 0, NULL, '192.168.0.103', '2017-03-07 09:52:39', '2017-03-02 01:49:00', '2017-03-07 09:52:39'),
(25, 0, 0, NULL, '老王', '15738361451', 'bcbe3365e6ac95ea2c0343a2395834dd', NULL, 0, 0, 1, 0, NULL, '192.168.0.116', '2017-03-10 07:04:49', '2017-03-02 02:46:24', '2017-03-10 07:04:49'),
(26, 0, 113, NULL, 'AI.何青', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-04 07:28:44', '2017-03-04 07:28:44'),
(27, 0, 114, NULL, '夕风', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-04 09:13:05', '2017-03-04 09:13:05'),
(28, 0, 115, NULL, '夕风', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-04 09:13:02', '2017-03-04 09:13:02'),
(29, 0, 116, NULL, '夕风', NULL, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-04 09:13:03', '2017-03-04 09:13:03'),
(30, 0, 0, NULL, 'ff', '13017553653', 'fae0b27c451c728867a567e8c1bb4e53', NULL, 0, 0, 1, 0, NULL, '192.168.0.101', '2017-03-10 08:08:43', '2017-03-06 10:52:02', '2017-03-10 08:08:43'),
(31, 0, 0, NULL, '小诗', '15603883302', 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-07 03:54:22', '2017-03-07 03:54:22'),
(32, 0, 0, NULL, 'congcong', '13073720275', '698d51a19d8a121ce581499d7b701668', NULL, 0, 0, 1, 0, NULL, '192.168.0.116', '2017-03-08 08:46:02', '2017-03-08 01:28:07', '2017-03-08 08:46:02'),
(33, 0, 120, NULL, 'ANN®', NULL, '', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-08 02:41:38', '2017-03-08 02:41:38'),
(34, 0, 0, NULL, 'Memphis', '13460369051', 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-08 08:08:12', '2017-03-08 08:08:12'),
(35, 0, 121, NULL, 'hello world', NULL, '', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-09 01:34:16', '2017-03-09 01:34:16'),
(36, 0, 0, NULL, '小灰', '18749860134', 'd41d8cd98f00b204e9800998ecf8427e', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-10 06:30:19', '2017-03-10 06:30:19'),
(37, 0, 0, NULL, 'hehe', '15670560752', 'bcbe3365e6ac95ea2c0343a2395834dd', NULL, 0, 0, 1, 0, NULL, '192.168.0.116', '2017-03-10 06:41:34', '2017-03-10 06:40:20', '2017-03-10 06:41:34'),
(38, 0, 122, NULL, 'Passerby°', NULL, '', NULL, 0, 0, 1, 0, NULL, NULL, '0000-00-00 00:00:00', '2017-03-13 08:53:30', '2017-03-13 08:53:30');

-- --------------------------------------------------------

--
-- 表的结构 `user_collection`
--

CREATE TABLE `user_collection` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `product_id` text CHARACTER SET latin1,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_collection`
--

INSERT INTO `user_collection` (`id`, `user_id`, `product_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 2, '3', 0, '2015-11-18 16:51:26', '0000-00-00 00:00:00'),
(4, 2, '3', 0, '2015-11-18 16:51:37', '0000-00-00 00:00:00'),
(5, 2, '3', 0, '2015-11-20 07:37:49', '0000-00-00 00:00:00'),
(6, 2, '3', 0, '2015-11-20 07:37:52', '0000-00-00 00:00:00'),
(7, 2, '3', 0, '2015-11-20 07:37:52', '0000-00-00 00:00:00'),
(8, 2, '3', 0, '2015-11-20 07:37:53', '0000-00-00 00:00:00'),
(9, 2, '3', 0, '2015-11-20 07:39:03', '0000-00-00 00:00:00'),
(10, 2, '3', 0, '2015-11-20 07:39:04', '0000-00-00 00:00:00'),
(11, 2, '3', 0, '2015-11-20 08:07:53', '0000-00-00 00:00:00'),
(12, 2, '3', 0, '2015-11-20 08:07:56', '0000-00-00 00:00:00'),
(13, 2, '3', 0, '2015-11-20 08:07:59', '0000-00-00 00:00:00'),
(14, 2, '3', 0, '2015-11-20 08:08:02', '0000-00-00 00:00:00'),
(15, 2, '3', 0, '2015-11-20 08:08:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `user_contact`
--

CREATE TABLE `user_contact` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `phone` text,
  `country_id` int(11) NOT NULL DEFAULT '1' COMMENT '国家',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '省份',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市',
  `district_id` int(11) DEFAULT NULL COMMENT '区域',
  `address` text COMMENT '详细地址',
  `remark` text COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_contact`
--

INSERT INTO `user_contact` (`id`, `user_id`, `name`, `phone`, `country_id`, `province_id`, `city_id`, `district_id`, `address`, `remark`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '雪贝特', '18538553590', 1, 2, 36, 500, '天安门', NULL, 0, '2017-03-03 07:51:31', '2017-03-03 10:43:18'),
(2, 26, 'ouba刚', '13273852162', 1, 11, 150, NULL, '许昌学院静十207111', NULL, 1, '2017-03-03 08:02:31', '2017-03-04 07:33:00'),
(3, 1, '老李', '13781120886', 1, 2, 36, 500, '就你隔壁', NULL, 1, '2017-03-03 08:04:04', '2017-03-04 07:25:07'),
(4, 1, '张然', '13854366985', 1, 35, 397, 517, '16753', NULL, 1, '2017-03-03 10:44:07', '2017-03-03 10:44:07'),
(9, 20, 'lee', '18336787690', 1, 11, 149, NULL, 'aaaa', NULL, 1, '2017-03-04 09:40:56', '2017-03-07 07:26:32'),
(10, 23, '呵呵哒', '1234', 1, 0, 0, NULL, '郑州金水区', NULL, 1, '2017-03-04 10:21:46', '2017-03-04 10:22:12'),
(11, 24, 'lee', '18336787690', 1, 2, 36, NULL, 'aaa', NULL, 1, '2017-03-07 03:55:29', '2017-03-07 04:05:01'),
(12, 25, 'apple', '15738361451', 1, 0, 0, NULL, '郑州金水区', NULL, 1, '2017-03-07 05:11:49', '2017-03-07 05:12:34'),
(13, 25, 'pear', '123456', 1, 0, 0, NULL, '南阳市fanf', NULL, 1, '2017-03-07 05:32:30', '2017-03-07 06:03:00'),
(14, 21, '雪贝特', '18538553590', 1, 2, 36, 500, '天安门', NULL, 1, '2017-03-07 07:50:37', '2017-03-07 07:53:28'),
(15, 21, '雪贝特', '18538553590', 1, 2, 4, 0, '开封市', NULL, 1, '2017-03-07 10:04:28', '2017-03-07 10:04:28'),
(16, 25, '123', '1231234124', 1, 0, 0, NULL, '123', NULL, 1, '2017-03-07 10:05:53', '2017-03-10 03:26:37'),
(17, 23, 'ouba刚', '18303741595', 1, 0, 0, NULL, '许昌学院', NULL, 1, '2017-03-07 10:15:24', '2017-03-07 10:15:24'),
(18, 33, '鸽子', '157', 1, 0, 0, NULL, '郑州', NULL, 1, '2017-03-08 04:10:37', '2017-03-08 04:10:37'),
(19, 32, 'congcong', '130', 1, 0, 0, NULL, 'zzzz', NULL, 1, '2017-03-08 08:30:08', '2017-03-08 08:30:25'),
(20, 30, '111', '111', 1, 0, 0, NULL, '111', NULL, 1, '2017-03-08 08:43:15', '2017-03-10 08:20:15'),
(21, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 08:51:29', '2017-03-08 08:51:29'),
(22, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 10:07:49', '2017-03-08 10:07:49'),
(23, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 10:10:05', '2017-03-08 10:10:05'),
(24, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 10:14:06', '2017-03-08 10:14:06'),
(25, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 10:17:25', '2017-03-08 10:17:25'),
(26, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 10:17:59', '2017-03-08 10:17:59'),
(27, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-08 10:27:01', '2017-03-08 10:27:01'),
(28, 30, '11', '22', 1, 0, 0, NULL, '33', NULL, 1, '2017-03-08 10:30:38', '2017-03-08 10:30:38'),
(29, 63, 'aa', '12344', 1, 0, 0, NULL, '郑州市', NULL, 1, '2017-03-10 02:57:24', '2017-03-10 02:57:24'),
(30, 59, '李四', '78945612303', 1, 0, 0, NULL, '索凌路', NULL, 1, '2017-03-10 03:06:15', '2017-03-10 03:06:15'),
(31, 37, 'hehe', '156', 1, 0, 0, NULL, '商丘', NULL, 1, '2017-03-10 06:42:29', '2017-03-10 06:42:29'),
(32, 30, '', '', 1, 0, 0, NULL, '', NULL, 1, '2017-03-10 08:17:37', '2017-03-10 08:17:37');

-- --------------------------------------------------------

--
-- 表的结构 `user_level`
--

CREATE TABLE `user_level` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `score` float NOT NULL DEFAULT '0' COMMENT '达到积分',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_level`
--

INSERT INTO `user_level` (`id`, `name`, `score`, `created_at`, `updated_at`) VALUES
(1, '基础会员', 0, '2017-02-15 09:58:33', '2017-03-02 09:42:02'),
(2, '初级会员', 50, '2016-12-26 15:53:37', '2017-02-15 10:37:28'),
(3, '白金会员', 100, '2017-01-05 23:03:20', '2017-02-15 10:37:37'),
(4, '铂金会员', 500, '2017-01-05 23:05:46', '2017-02-15 10:37:53'),
(5, '黄金会员', 1000, '2017-03-02 07:12:29', '2017-03-02 07:13:00');

-- --------------------------------------------------------

--
-- 表的结构 `user_msg`
--

CREATE TABLE `user_msg` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_msg`
--

INSERT INTO `user_msg` (`id`, `user_id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'welcome', 'huanyinglaidaoyingxionglianmeng', 0, '2016-12-25 15:50:53', '2017-02-14 17:34:00'),
(2, 2, 'welcome', 'huanyinglaidaoyingxionglianmeng', 1, '2016-12-25 16:12:51', '2017-02-09 14:45:52'),
(3, 1, '111', '1111', 1, '2016-12-26 09:53:11', '2017-02-14 17:34:00'),
(4, 2, 'q', 'q', 1, '2016-12-26 09:53:52', '2017-02-05 21:35:39'),
(5, 2, '111', '222', 0, '2017-01-04 14:28:28', '2017-02-08 09:17:01'),
(7, 1, '1', '1', 1, '2017-01-05 03:30:40', '2017-02-14 17:34:00'),
(8, 1, 'elm', '123456', 1, '2017-01-05 22:53:18', '2017-02-14 17:34:00'),
(9, 1, '11', '112233', 1, '2017-01-05 22:53:55', '2017-02-11 09:05:50'),
(10, 1, '666', '666', 1, '2017-01-05 23:05:13', '2017-02-02 11:08:56'),
(11, 1, '1', '1', 1, '2017-01-06 03:27:46', '2017-02-05 21:35:42'),
(12, 1, '你好吗', '你好吗？我是wemall', 1, '2017-01-06 18:20:11', '2017-02-03 07:02:57'),
(13, 1, '111', '58498', 1, '2017-01-06 18:24:40', '2017-02-09 14:45:52'),
(14, 1, '545', '8        6 ', 1, '2017-01-06 18:25:13', '2017-02-08 09:15:37'),
(15, 1, '1', '1', 1, '2017-01-06 23:30:14', '2017-02-14 17:34:00'),
(16, 1, '1111', '1111', 1, '2017-01-08 18:47:12', '2017-02-14 17:34:00'),
(17, 1, '333122', '331', 1, '2017-01-12 19:54:49', '2017-02-15 13:02:49');

-- --------------------------------------------------------

--
-- 表的结构 `user_tx`
--

CREATE TABLE `user_tx` (
  `id` int(10) unsigned NOT NULL,
  `txid` text,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `account` text COMMENT '提现账户',
  `money` float NOT NULL DEFAULT '0',
  `fee` float NOT NULL DEFAULT '0' COMMENT '手续费',
  `tx` float NOT NULL DEFAULT '0' COMMENT '最终提现',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:未审核，1:通过，2:完成，-1:拒绝',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_tx`
--

INSERT INTO `user_tx` (`id`, `txid`, `user_id`, `account`, `money`, `fee`, `tx`, `status`, `created_at`, `updated_at`) VALUES
(4, '31701101145067', 1, '641785852@qq.com', 29.33, 0.29, 29.04, 1, '2017-01-09 19:45:06', '2017-01-09 22:50:33'),
(5, '31701131051267', 2, '7777', 702, 7.02, 694.98, -1, '2017-01-12 18:51:26', '2017-01-12 18:51:47'),
(6, '31701131418167', 1, '30', 58.55, 0.59, 57.96, 1, '2017-01-12 22:18:16', '2017-01-12 22:18:51'),
(7, '31701141433080', 1, '1', 99.85, 1, 98.85, -1, '2017-01-13 22:33:08', '2017-01-13 22:33:20'),
(8, '31701171748261', 1, '969969', 2, 1, 98.85, 1, '2017-01-17 01:48:26', '2017-01-17 19:35:40'),
(9, '31702131105523', 1, '5555555555555', 15.53, 0.16, 15.37, -1, '2017-02-12 19:05:52', '2017-02-12 19:07:28');

-- --------------------------------------------------------

--
-- 表的结构 `wx_config`
--

CREATE TABLE `wx_config` (
  `id` int(5) unsigned NOT NULL,
  `token` text,
  `appid` text,
  `appsecret` text,
  `encodingaeskey` text,
  `x_appid` text COMMENT '小程序',
  `x_appsecret` text COMMENT '小程序',
  `old_id` text COMMENT '原始id',
  `switch` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_config`
--

INSERT INTO `wx_config` (`id`, `token`, `appid`, `appsecret`, `encodingaeskey`, `x_appid`, `x_appsecret`, `old_id`, `switch`, `created_at`, `updated_at`) VALUES
(1, 'wemall', 'wx6d040141df50d2a3', '523c93731918e8476654ca8f73133824', 'vkG6JOKy7f2f1nejqJFlOJkjJEK5JJlNaJjjSQ6Q2gM', 'wx5f1a51823b837fe8', '8e157d6823fb72dcb17f9762308b8333', 'gh_6f79b1a839f6', 1, '2016-01-05 02:16:16', '2017-03-15 06:23:00');

-- --------------------------------------------------------

--
-- 表的结构 `wx_kefu`
--

CREATE TABLE `wx_kefu` (
  `id` int(5) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `kefu` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_kefu`
--

INSERT INTO `wx_kefu` (`id`, `status`, `kefu`, `created_at`, `updated_at`) VALUES
(1, 0, 'biyuehun', '0000-00-00 00:00:00', '2017-03-02 06:50:24');

-- --------------------------------------------------------

--
-- 表的结构 `wx_menu`
--

CREATE TABLE `wx_menu` (
  `id` int(5) unsigned NOT NULL,
  `pid` int(5) NOT NULL DEFAULT '0',
  `type` text,
  `name` text,
  `key` text,
  `url` text,
  `rank` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_menu`
--

INSERT INTO `wx_menu` (`id`, `pid`, `type`, `name`, `key`, `url`, `rank`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 0, 'view', '商业版', '111', 'http://www.wemallshop.com/wemall/index.php/App/Index/index', '1', 0, '2', '2016-02-18 06:46:22', '2017-01-11 10:28:43'),
(2, 1, 'view', '分销版', '', 'http://www.wemallshop.com/wfx/App/Shop/index', '4', 0, '1213', '2015-11-06 09:25:28', '2017-02-16 10:17:00'),
(3, 0, 'click', 'QQ客服', 'qqkf', '', '3', 0, '2034210985', '2015-12-31 08:19:22', '2017-01-12 06:38:34');

-- --------------------------------------------------------

--
-- 表的结构 `wx_print`
--

CREATE TABLE `wx_print` (
  `id` int(11) unsigned NOT NULL,
  `apikey` varchar(100) DEFAULT NULL COMMENT 'apikey',
  `mkey` varchar(100) DEFAULT NULL COMMENT '秘钥',
  `partner` varchar(100) DEFAULT NULL COMMENT '用户id',
  `machine_code` varchar(100) DEFAULT NULL COMMENT '机器码',
  `switch` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_print`
--

INSERT INTO `wx_print` (`id`, `apikey`, `mkey`, `partner`, `machine_code`, `switch`, `created_at`, `updated_at`) VALUES
(1, '61', '31', '16', '16', 0, '2016-08-07 11:49:22', '2017-03-02 06:50:36');

-- --------------------------------------------------------

--
-- 表的结构 `wx_reply`
--

CREATE TABLE `wx_reply` (
  `id` int(10) unsigned NOT NULL,
  `type` text,
  `title` text,
  `description` text,
  `file_id` int(11) NOT NULL DEFAULT '0',
  `url` text,
  `key` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_reply`
--

INSERT INTO `wx_reply` (`id`, `type`, `title`, `description`, `file_id`, `url`, `key`, `remark`, `created_at`, `updated_at`) VALUES
(1, 'news', '恭喜你加入WeMall，欢迎体验WeMall商业版，WeMall分销版和WeMall开源版。WeMall商业版更新，速度提升30%，致力于打造世界上最快，体验最好的微商城。客服QQ：2034210985', '1111', 29, '', 'subscribe', '1212', '2016-01-05 02:19:53', '2017-03-02 06:49:04'),
(2, 'news', '欢迎来到商业版wemall商城', '欢迎来到商业版wemall商城11111', 103, 'http://www.wemallshop.com/3/App/Index/index', '商城', '', '2016-01-05 02:23:41', '2017-03-02 06:49:49'),
(3, 'news', '2222222', '111', 103, '1111', '11111', '1111', '2017-01-12 09:27:57', '2017-03-02 06:49:41');

-- --------------------------------------------------------

CREATE TABLE `robot` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `apikey` varchar(64) DEFAULT NULL,
  `userid` varchar(64) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1开启，0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `robot` VALUES ('1', '889ec96903fc4948845497850c3ede23', '2ed3d583c834e94f1', null, '2018-07-09 16:21:43', null, '1');

--
-- 表的结构 `wx_tplmsg`
--

CREATE TABLE `wx_tplmsg` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `type` text,
  `title` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `remark` text,
  `template_id_short` text,
  `template_id` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_tplmsg`
--

INSERT INTO `wx_tplmsg` (`id`, `name`, `type`, `title`, `status`, `remark`, `template_id_short`, `template_id`, `created_at`, `updated_at`) VALUES
(1, '订单提醒(新订单通知)', 'order', '尊敬的客户,您的订单已成功提交！', 1, '666', 'OPENTM201785396', '', '2016-08-07 11:50:16', '2017-01-12 23:07:32'),
(2, '支付提醒(订单支付成功通知)', 'pay', '您已成功支付', 1, '3333', 'OPENTM207791277', '', '2016-08-07 11:50:16', '2017-01-12 23:07:35'),
(3, '发货提醒(订单发货提醒)', 'delivery', '尊敬的客户,您的订单已发货！', 1, '33312', 'OPENTM207763419', '', '2016-08-07 11:50:16', '2017-02-16 11:04:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_position`
--
ALTER TABLE `ads_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analysis`
--
ALTER TABLE `analysis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group_access`
--
ALTER TABLE `auth_group_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_tpl`
--
ALTER TABLE `fee_tpl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`pid`),
  ADD KEY `region_type` (`type`);

--
-- Indexes for table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_tpl`
--
ALTER TABLE `mail_tpl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_applet`
--
ALTER TABLE `oauth_applet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_wx`
--
ALTER TABLE `oauth_wx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_contact`
--
ALTER TABLE `order_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_fee`
--
ALTER TABLE `order_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_feedback_type`
--
ALTER TABLE `order_feedback_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pay_code` (`type`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_comment`
--
ALTER TABLE `product_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_exchange`
--
ALTER TABLE `product_exchange`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_label`
--
ALTER TABLE `product_label`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sku`
--
ALTER TABLE `sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_tpl`
--
ALTER TABLE `sms_tpl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_verify`
--
ALTER TABLE `sms_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trade`
--
ALTER TABLE `trade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_collection`
--
ALTER TABLE `user_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contact`
--
ALTER TABLE `user_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_msg`
--
ALTER TABLE `user_msg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tx`
--
ALTER TABLE `user_tx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_config`
--
ALTER TABLE `wx_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_kefu`
--
ALTER TABLE `wx_kefu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_menu`
--
ALTER TABLE `wx_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_print`
--
ALTER TABLE `wx_print`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_reply`
--
ALTER TABLE `wx_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_tplmsg`
--
ALTER TABLE `wx_tplmsg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ads_position`
--
ALTER TABLE `ads_position`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `analysis`
--
ALTER TABLE `analysis`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_group_access`
--
ALTER TABLE `auth_group_access`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `auth_rule`
--
ALTER TABLE `auth_rule`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fee_tpl`
--
ALTER TABLE `fee_tpl`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mail_tpl`
--
ALTER TABLE `mail_tpl`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oauth_applet`
--
ALTER TABLE `oauth_applet`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oauth_wx`
--
ALTER TABLE `oauth_wx`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `order_contact`
--
ALTER TABLE `order_contact`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `order_fee`
--
ALTER TABLE `order_fee`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_feedback_type`
--
ALTER TABLE `order_feedback_type`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `product_comment`
--
ALTER TABLE `product_comment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_exchange`
--
ALTER TABLE `product_exchange`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_label`
--
ALTER TABLE `product_label`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `sku`
--
ALTER TABLE `sku`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sms_tpl`
--
ALTER TABLE `sms_tpl`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sms_verify`
--
ALTER TABLE `sms_verify`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `trade`
--
ALTER TABLE `trade`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `user_collection`
--
ALTER TABLE `user_collection`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user_contact`
--
ALTER TABLE `user_contact`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_msg`
--
ALTER TABLE `user_msg`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user_tx`
--
ALTER TABLE `user_tx`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `wx_config`
--
ALTER TABLE `wx_config`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wx_kefu`
--
ALTER TABLE `wx_kefu`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wx_menu`
--
ALTER TABLE `wx_menu`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wx_print`
--
ALTER TABLE `wx_print`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wx_reply`
--
ALTER TABLE `wx_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wx_tplmsg`
--
ALTER TABLE `wx_tplmsg`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
